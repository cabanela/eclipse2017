#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Experiment Spectrum Plot
Created on Mon August 28, 2017 03:24 PM
Author: Juan Cabanela

This Python script reads the FITS files output by my experiment code and will 
attempt to plot up the spectra and save them to individual files (maybe even
make a movie using ffmpeg).
"""

import os
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import specplotlib as splot
import explib as exper
from astropy.table import Table, Column


#
# Configure Program Settings
#


# Greeting and attach to devices
print("")
print("Eclipse 2017 Experimental Spectra Plots")
print("=======================================")

# Load the eclipse circumstances data
circdata = exper.get_eclipse_circumstances()

# Get the experimental datafile
filename = exper.get_experimental_datafile()

# Read in the data
print(" > Loading datafile ...", end='')
full_datable = Table.read(filename, format='fits')
print(" DONE!")

# Make a copy for computing count rates
print(" > Building reduced data table (including count rates) ...", end='')
ratestable = exper.build_ratestable(full_datable)
print(" DONE!")

# Build tables that are groups into wavelength data and other data
by_datatype = ratestable.group_by('datatype')
data_mask = by_datatype.groups.keys['datatype'] == 'data'
wl_mask = by_datatype.groups.keys['datatype'] == 'wavelength'

# Built "experimental" and "Wavelength" data tables in memory
exp_datatable = by_datatype.groups[data_mask]
wl_datatable = by_datatype.groups[wl_mask]

# Load wavelength and spectral data into arrays, leave the rest in the
# astropy table
wavelengths = wl_datatable['spectrum'][0]  # 1D Array
spectra = np.array(exp_datatable['spectrum'])  # 2D Array


## Loop through all the spectra and plot them, saving the results to individual
## PNG files.
# Define data directory for processing
data_dir = exper.get_data_dir()
for i in range(len(exp_datatable)):
    print("Plotting spectra for loop ", exp_datatable['loop'][i])
    print("lux: ", exp_datatable['lux'][i])
    print("exp_time: ", exp_datatable['exposure'][i])
    print("99th_percentile: ", exp_datatable['99th_percentile_spectra'][i])
    print("99th_percentile rate: ", exp_datatable['99th_percentile_counts'][i])
    if (exp_datatable['exposure'][i] > 0):
        plt.clf()
        #splot.spectraplot(wavelengths, spectra[i])

        # Remove any portion of this spectrum that is saturated
        unsaturated_mask= spectra[i]<3900
        clean_spectra = spectra[i][unsaturated_mask]
        clean_wavelengths = wavelengths[unsaturated_mask]
        splot.spectraplot_noshow(clean_wavelengths, clean_spectra)
        
        # Create plot title to include time
        timestamp = exp_datatable['UTC_Spec'][i]
        year = timestamp[0:4]
        mon = timestamp[4:6]
        day = timestamp[6:8]
        hour = timestamp[9:11]
        minute = timestamp[11:13]
        sec = timestamp[13:18]
        date = "{0:2s}/{1:2s}/{2:4s}".format(mon, day, year)
        time = "{0:2s}:{1:2s}:{2:5s}".format(hour, minute, sec)
        titlestr = "Overhead Spectrum at {0:s} on {1:s} (UTC)".format(time, date)
        plt.title(titlestr)
        
        # Label the SQM reading
        ypos1 = exp_datatable['99th_percentile_spectra'][i]*0.8
        ypos2 = exp_datatable['99th_percentile_spectra'][i]*0.7
        xpos = 800
        plt.text(xpos, ypos1, "SQM: {0:5.2f}".format(exp_datatable['mpsas'][i]))
        plt.text(xpos, ypos2, "lux: {0:.1f}".format(exp_datatable['lux'][i]))
        
        # Save plot
        filestub = "spectra_{0:4s}{1:2s}{2:2s}{3:2s}{4:2s}{5:02d}.png".format(year, mon, day, hour, minute, int(float(sec)))
        filename = data_dir+filestub
        print("Saving spectra to ", filestub)
        #plt.savefig(filename)
        plt.show()
    else:
        print("NOTE: No spectrum to plot.")
    