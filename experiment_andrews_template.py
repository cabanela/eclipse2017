#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Template Maker
Created on Wed February 21, 2018 10:46 AM
Author: Andrew Louwagie Gordon (based on datareader by Juan Cabanela)

This Python script reads the FITS files output by my experiment code...

It doesn't do much yet, it is really meant as a shell for later development of
python scripts for data analysis.
"""

import os
import numpy as np
from astropy.table import Table, Column
import explib as exper
import matplotlib.pyplot as plt

# Greeting and attach to devices
print("")
print("Eclipse 2017 Template Maker")
print("===========================")

# Get the reduced FITS datafile filename
filename = exper.get_reduced_datafile()

# Read in the original experimental data into astropy table
print(" > Loading datafile ...", end='')
full_datable = Table.read(filename, format='fits')
print(" DONE!")

# Build tables that are groups into wavelength data and other data
by_datatype = full_datable.group_by('datatype')
data_mask = by_datatype.groups.keys['datatype'] == 'data'
wl_mask = by_datatype.groups.keys['datatype'] == 'wavelength'

exp_datatable = by_datatype.groups[data_mask]
wl_datatable = by_datatype.groups[wl_mask]

# Load wavelength and spectral data into arrays, leave the rest in the
# astropy table
wavelengths = wl_datatable['spectrum'][0]  # 1D Array
spectra = np.array(exp_datatable['spectrum'])  # 2D Array
scaled_spectra1 = np.array(exp_datatable['spectra_scaled_to_max'])  # 2D Array
scaled_spectra2 = np.array(exp_datatable['spectra_scaled_to_99th'])  # 2D Array

'''
Andrew's modifications
-------------------------------------------------------------------------------
'''

# Generate a set of lists to reference for plotting.
template_spectrum_max = []
template_spectrum_99th = []

# This value is unimportant, it just allowed me to note how many data points were removed. 
num_removed = 0

# These statements grab the cloudless, unobscured spectra.
    # This code assumes only spectra with no cloud cover whatsoever.
for n in range(len(exp_datatable)):
    if exp_datatable['Obsc'][n] == 0 and exp_datatable['clouds'][n] == 0:
        template_spectrum_max.append(exp_datatable['spectra_scaled_to_max'][n])
    else:
        num_removed += 1        
        
for n in range(len(exp_datatable)):
    if exp_datatable['Obsc'][n] == 0 and exp_datatable['clouds'][n] == 0:
        template_spectrum_99th.append(exp_datatable['spectra_scaled_to_99th'][n])

template_spectrum_max_sum = np.sum(template_spectrum_max, axis = 0)
template_spectrum_99th_sum = np.sum(template_spectrum_99th, axis = 0)

template_spectrum_max_sum /= len(template_spectrum_max)
template_spectrum_99th_sum /= len(template_spectrum_99th)

plt.plot(wavelengths, template_spectrum_max_sum)
plt.xlabel("$\lambda$ (nm)")
plt.ylabel("Scaled Spectrum")
plt.title("Template Spectrum Scaled to Max")
plt.show()

plt.plot(wavelengths, template_spectrum_99th_sum)
plt.xlabel("$\lambda$ (nm)")
plt.ylabel("Scaled Spectrum")
plt.title("Template Spectrum Scaled to 99th")
plt.show()

'''
-------------------------------------------------------------------------------
'''
      