#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Experiment Spectrum Fit
Created on Fri Aug 18, 2017 09:21 AM
Author: Juan Cabanela

This Python script reads the FITS files output by my experiment code and will 
attempt to fit the spectrum with a blackbody curve.

August 18, 2017: Made an initial attempt to fit the spectra.  Fitting doesn't
  appear to be working, even with scaling of the blackbody.
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import specplotlib as splot
import explib as exper
from astropy.table import Table, Column
from scipy.optimize import curve_fit


def func(lam, T, scale):
    """ Scaled Blackbody as a function of wavelength (um) and temperature (K)
    
        Designed to allow fitting of Planck_curve with scaling since I
        don't know what units the spectrometer is really operating in
        given unclear steradians.
    """
    return scale*Planck_curve(lam, T)


def Planck_curve(lam, T):
    """ Blackbody as a function of wavelength (um) and temperature (K).

    return units of erg/s/cm^2/cm/Steradian,
    but I allowed a 'scale' to be applied since the units of the original
    spectrometer are unclear.
    
    """
    from scipy.constants import h, k, c
    lam = 1e-6 * lam  # convert to meters
    return 2*h*c**2 / (lam**5 * (np.exp(h*c / (lam*k*T)) - 1))

# Greeting and attach to devices
print("")
print("Eclipse 2017 Experiment Spectra Fitting (Beta)")
print("==============================================")

# Get the experimental datafile
filename = exper.get_experimental_datafile()

# Read in the data
print(" > Loading datafile ...", end='')
full_datable = Table.read(filename, format='fits')
print(" DONE!")

# Make a copy for computing count rates
print(" > Building reduced data table (including count rates) ...", end='')
ratestable = exper.build_ratestable(full_datable)
print(" DONE!")

# Build tables that are groups into wavelength data and other data
by_datatype = ratestable.group_by('datatype')
data_mask = by_datatype.groups.keys['datatype'] == 'data'
wl_mask = by_datatype.groups.keys['datatype'] == 'wavelength'

exp_datatable = by_datatype.groups[data_mask]
wl_datatable = by_datatype.groups[wl_mask]

# Track only those entries where spectra were taken
spec_mask = exp_datatable['exposure'] > 0
spec_datatable = exp_datatable[spec_mask]

# Load wavelength and spectral data into arrays, leave the rest in the
# astropy table
wavelengths = wl_datatable['spectrum'][0]  # 1D Array
spectra = np.array(exp_datatable['spectrum'])  # 2D Array

# Testbed the code using the second spectra (which has some saturated points)
i=2
unsaturated_mask= spectra[i]<3900
clean_spectra = spectra[i][unsaturated_mask]
clean_wavelengths = wavelengths[unsaturated_mask]/1000
splot.spectraplot_um_noshow(clean_wavelengths, clean_spectra)
plt.xlabel("Wavelength (um)")

# Try to curve fit this

# Estimate the scaling factor
estT = 6000   # initial temperature estimate
max_data = np.max(clean_spectra)
test_Planck = func(clean_wavelengths, estT, 1)
max_Planck = np.max(test_Planck)
estScale = max_data/max_Planck
test_Planck *= estScale   # Generate a test spectrum
plt.plot(clean_wavelengths, test_Planck, 'k-', label="test fit")

## Perform the fit
#popt, pcov = curve_fit(func, clean_wavelengths, clean_spectra, p0=(estT, estScale))
#(bestT, bestscale) = popt
#(sigmaT, sigmascale) = np.sqrt(np.diag(pcov))
#ybest = func(clean_wavelengths, bestT, bestscale)
#plt.plot(clean_wavelengths, ybest, 'k-', label="best fit")
#
#print('Parameters of best-fitting model:')
#print('     T = %7.2f +/- %.2f' % (bestT, sigmaT))
#print(' scale = %7.2g +/- %.2g' % (bestscale, sigmascale))
#
#degrees_of_freedom = len(clean_wavelengths) - 2
#resid = (clean_spectra - func(clean_wavelengths, *popt))
#chisq = np.dot(resid, resid)
#
#print(degrees_of_freedom, 'dof')
#print('chi squared %.2f' % chisq)
#print('nchi2 %.2f' % (chisq / degrees_of_freedom))

plt.show()

### Loop through all the spectra first (skip the zero spectra)
#for i in range(len(exp_datatable)):
#    print("Plotting spectra for loop ", exp_datatable['loop'][i])
#    print("lux: ", exp_datatable['lux'][i])
#    print("exp_time: ", exp_datatable['exposure'][i])
#    #print("99th_percentile: ", exp_datatable['99th_percentile_spectra'][i])
#    #print("99th_percentile rate: ", exp_datatable['99th_percentile_counts'][i])
#    if (exp_datatable['exposure'][i] > 0):
#        plt.clf()
#        #spec.spectraplot(wavelengths, spectra[i])
#
#        # Remove any portion of this spectrum that is saturated
#        unsaturated_mask= spectra[i]<3900
#        clean_spectra = spectra[i][unsaturated_mask]
#        clean_wavelengths = wavelengths[unsaturated_mask]/1000
#        spec.spectraplot_um_noshow(clean_wavelengths, clean_spectra)
#        plt.xlabel("Wavelength (um)")
#        
#        # Try to curve fit this
#        
#        # Estimate the scaling factor
#        estT = 6000   # initial temperature estimate
#        max_data = np.max(clean_spectra)
#        max_Planck = np.max(func(clean_wavelengths, estT, 1))
#        estScale = max_data/max_Planck
#        
#        # Perform the fit
#        popt, pcov = curve_fit(func, clean_wavelengths, clean_spectra, p0=(estT, estScale))
#        (bestT, bestscale) = popt
#        (sigmaT, sigmascale) = np.sqrt(np.diag(pcov))
#        ybest = func(clean_wavelengths, bestT, bestscale)
#        plt.plot(clean_wavelengths, ybest, 'k-', label="best fit")
#        
#        print('Parameters of best-fitting model:')
#        print('     T = %7.2f +/- %.2f' % (bestT, sigmaT))
#        print(' scale = %7.2g +/- %.2g' % (bestscale, sigmascale))
#
#        degrees_of_freedom = len(clean_wavelengths) - 2
#        resid = (clean_spectra - func(clean_wavelengths, *popt))
#        chisq = np.dot(resid, resid)
#
#        print(degrees_of_freedom, 'dof')
#        print('chi squared %.2f' % chisq)
#        print('nchi2 %.2f' % (chisq / degrees_of_freedom))
#
#        plt.show()
#        
#    else:
#        print("NOTE: No spectrum to plot.")
    
        