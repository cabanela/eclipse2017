#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Experiment Data Processing 
Created on Mon October 16, 2017 03:37 PM
Author: Juan Cabanela

This Python script reads the FITS files output by my experiment code and will 
be used to play with the data and try to build a master datafile that can be
used for the final data processing.  

Added features:
   - Importation and matching of eclipse circumstances.
   - Rescaling of spectra to maximum and the 99th percentile counts.
   - Added RGB colors from fisheye photos
   - Added 'clouds' record from visual inspection of fisheye photos.
"""

import os
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import specplotlib as splot
import explib as exper
from astropy.table import Table, Column, join
from astropy.time import Time
import astropy.units as u
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, get_sun
from scipy.interpolate import interp1d

# Greeting 
print("")
print("Eclipse 2017 Experimental Data Processing")
print("=========================================")

# Get the experimental datafile
filename = exper.get_experimental_datafile()

# Read in the data
print(" > Loading datafile ...", end='')
full_datable = Table.read(filename, format='fits')
print(" DONE!")

# Make a copy for computing count rates and in the process generate timestamp
# columns
print(" > Building reduced data table (including count rates) ...", end='')
ratestable = exper.build_ratestable(full_datable)
print(" DONE!")

#
# Process the rates table to include spectra scaled by the maximum.
# This segement of code is based on code by Andrew Louwagie-Gordon.
#
print(" > Building rescaled spectra columns ...", end='')
# Load spectral data into numpy array
spectra = np.array(ratestable['spectrum'][1:]) # 2D Array (skipping first line which is wavelength)
nnpercentile = np.array(ratestable['99th_percentile_spectra'][1:]) # array of 99th percentile values
maxes = np.max(spectra, axis=1) # Get the max of each spectra
rescaled_to_max = spectra/maxes[:,np.newaxis]  # Rescale each spectra to max
rescaled_to_99th = spectra/nnpercentile[:,np.newaxis]

# Copy the rescaled spectra to column, skipping first row which is wavelengths
newspectra_max = ratestable['spectrum'].copy()
newspectra_max.name='spectra_scaled_to_max'
newspectra_nn = ratestable['spectrum'].copy()
newspectra_nn.name='spectra_scaled_to_99th'
ratestable.add_column(newspectra_max)
ratestable.add_column(newspectra_nn)
ratestable['spectra_scaled_to_max'][1:] = rescaled_to_max
ratestable['spectra_scaled_to_99th'][1:] = rescaled_to_99th
print(" DONE!")

#
# Merge in information about the eclipse circumstances
#
print(" > Merging in eclipse circumstances data ...", end='')
ratestable.rename_column('timestamp_MidSpec', 'timestamp') 

# Load the eclipse circumstances data
# NOTE: Magnitude of eclipse is the fraction of the angular diameter of the
# Sun being eclipsed.  It may make more sense to use Obsuration percentage
circdata = exper.get_eclipse_circumstances()

# join is apparently not matching some of my data because some of the 
# observations were from before when I have circumstances data
merged_table_raw = join(ratestable, circdata, join_type='left')

# Clean up observations to add computed altitude and azimuth of Sun and 
# mark non-eclipse circumstances as 'zero' which is reasonable for obscuration
# and magnitude
merged_table = merged_table_raw.filled(0)

# Determine altitude and azimuth at missing times
ravenna_lake_sp = EarthLocation(lat=41*u.deg+01.2383*u.arcmin, lon=-98*u.deg+53.3333*u.arcmin, height=608.8*u.m)
ts_list = list(merged_table['timestamp'])
time2str = lambda x: datetime.datetime.fromtimestamp(x).strftime("%Y-%m-%d %H:%M:%S")
utc_times = Time(np.array(list(map(time2str, ts_list))))
ravenna_frame = AltAz(obstime=utc_times, location=ravenna_lake_sp)
sun_loc = get_sun(utc_times).transform_to(ravenna_frame)
# Assign values for altitude and azimuth as computed for exact times
merged_table['Azm'] = sun_loc.az.to('deg').value
merged_table['Alt'] = sun_loc.alt.to('deg').value

#
# Merge sky cloud cover information
#
clouddata = exper.get_cloudcover_info()
# interpolate cloud cover between the minutes
cloud_interp = interp1d(clouddata['timestamp'], clouddata['clouds'], kind='linear', bounds_error=False, fill_value=-1)
merged_table['clouds'] = cloud_interp(ts_list)

#
# Merge in sky color information from the series of fisheye images
#
colordata = exper.get_fisheye_information()

# Interpolate timestamps 1 second apart
#ts_last = colordata['timestamp'][-1]
#ts_first = colordata['timestamp'][0]
#steps = ts_last-ts_first+1
#ts = np.linspace(ts_first, ts_last, num=steps, endpoint=True)
#dates_ts = [datetime.datetime.fromtimestamp(t) for t in ts]

# Interpolate fisheye color data
R_interp = interp1d(colordata['timestamp'], colordata['R_avg'], kind='cubic', bounds_error=False, fill_value=-1)
G_interp = interp1d(colordata['timestamp'], colordata['G_avg'], kind='cubic', bounds_error=False, fill_value=-1)
B_interp = interp1d(colordata['timestamp'], colordata['B_avg'], kind='cubic', bounds_error=False, fill_value=-1)
merged_table['R_interp'] = R_interp(ts_list)
merged_table['G_interp'] = G_interp(ts_list)
merged_table['B_interp'] = B_interp(ts_list)
print(" DONE!")

#
# Consider adding rescaling of RGB colors to the blue value
#

merged_table['R_scaled2B'] = R_interp(ts_list) / B_interp(ts_list)
merged_table['G_scaled2B'] = G_interp(ts_list) / B_interp(ts_list)
merged_table['B_scaled2B'] = B_interp(ts_list) / B_interp(ts_list)

merged_table['R_scaled2G'] = R_interp(ts_list) / G_interp(ts_list)
merged_table['G_scaled2G'] = G_interp(ts_list) / G_interp(ts_list)
merged_table['B_scaled2G'] = B_interp(ts_list) / G_interp(ts_list)

merged_table['R_scaled2R'] = R_interp(ts_list) / R_interp(ts_list)
merged_table['G_scaled2R'] = G_interp(ts_list) / R_interp(ts_list)
merged_table['B_scaled2R'] = B_interp(ts_list) / R_interp(ts_list)

# 
# Save the merged datafile
#
merged_fullpath = exper.get_reduced_datafile()
merged_table.write(merged_fullpath, format='fits', overwrite=True)

#
# Now start building subsets of the complete data tables for plotting purposes
#
# Build tables that are groups into wavelength data and other data
by_datatype = merged_table.group_by('datatype')
data_mask = by_datatype.groups.keys['datatype'] == 'data'
wl_mask = by_datatype.groups.keys['datatype'] == 'wavelength'

# Built "experimental" and "Wavelength" data tables in memory
exp_datatable = by_datatype.groups[data_mask]
wl_datatable = by_datatype.groups[wl_mask]

# Load wavelength and spectral data into arrays, leave the rest in the
# astropy table
wavelengths = wl_datatable['spectrum'][0]  # 1D Array
spectra = np.array(exp_datatable['spectrum'])  # 2D Array
scaled_spectra1 = np.array(exp_datatable['spectra_scaled_to_max'])  # 2D Array
scaled_spectra2 = np.array(exp_datatable['spectra_scaled_to_99th'])  # 2D Array

# Test the scaled spectra code
for i in range(len(exp_datatable) - 1):
    # Testing, uncomment these to see each individual spectra
    plt.plot(wavelengths, scaled_spectra1[i])
    plt.xlabel("$\lambda$ (nm)")
    plt.ylabel("Scaled Spectrum")
    plt.title("All Sky Spectra (Scaled to Max)")
plt.show()

# Test the scaled spectra code
for i in range(len(exp_datatable) - 1):
    # Testing, uncomment these to see each individual spectra
    plt.plot(wavelengths, scaled_spectra2[i])
    plt.xlabel("$\lambda$ (nm)")
    plt.ylabel("Scaled Spectrum")
    plt.title("All Sky Spectra (Scaled to 99th Percentile)")
plt.show()

# Plot sky flux versus time
dates_SQM = [datetime.datetime.fromtimestamp(t) for t in exp_datatable['timestamp_SQM']]

fig, ax = plt.subplots()
plt.plot(dates_SQM, exp_datatable['mpsas'])
plt.gca().invert_yaxis()
plt.xlabel("UTC time")
plt.ylabel("Flux (mpsas)")
plt.title("Flux versus Time (magnitudes per square arcsecond)")
labels = ax.get_xticklabels()
formatter = mdates.DateFormatter('%H:%M:%S')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.setp(labels, rotation=30, fontsize=10)
plt.show()

fig, ax = plt.subplots()
plt.plot(dates_SQM, exp_datatable['lux'])
plt.xlabel("UTC time")
plt.ylabel("Flux (lux)")
plt.title("Flux versus Time (lux)")
labels = ax.get_xticklabels()
formatter = mdates.DateFormatter('%H:%M:%S')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.setp(labels, rotation=30, fontsize=10)
plt.show()

fig, ax = plt.subplots()
plt.plot(dates_SQM, exp_datatable['Temp'])
plt.xlabel("UTC time")
plt.ylabel("Temperature (C)")
plt.title("Temperature versus Time (lux)")
labels = ax.get_xticklabels()
formatter = mdates.DateFormatter('%H:%M:%S')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.setp(labels, rotation=30, fontsize=10)
plt.show()

# Plot data by eclipse magnitude
#during_eclipse = exp_datatable['Magn']>0

fig, ax = plt.subplots()
plt.plot(exp_datatable['Magn'], exp_datatable['lux'])
plt.xlabel("Eclipse Magnitude")
plt.ylabel("Flux (lux)")
plt.title("Flux versus Eclipse Magnitude")
plt.show()

fig, ax = plt.subplots()
plt.plot(exp_datatable['Obsc'], exp_datatable['lux'])
plt.xlabel("Eclipse Obscuration")
plt.ylabel("Flux (lux)")
plt.title("Flux versus Eclipse Obscuration")
plt.show()

fig, ax = plt.subplots()
plt.plot(exp_datatable['Obsc'], exp_datatable['Magn'])
plt.xlabel("Eclipse Obscuration")
plt.ylabel("Eclipse Magnitude")
plt.title("Eclipse Obscuration versus Magnitude")
plt.show()

fig, ax = plt.subplots()
plt.plot(exp_datatable['Obsc'], exp_datatable['R_interp'], 'r-', label="R")
plt.plot(exp_datatable['Obsc'], exp_datatable['G_interp'], 'g-', label="G")
plt.plot(exp_datatable['Obsc'], exp_datatable['B_interp'], 'b-', label="B")
plt.plot(exp_datatable['Obsc'], exp_datatable['clouds']*100, 'k-', label="Clouds")
plt.xlabel("Eclipse Obscuration")
plt.ylabel("Color Intensity")
plt.title("Eclipse Obscuration versus Fisheye Color")
plt.show()

fig, ax = plt.subplots()
plt.plot(exp_datatable['Obsc'], exp_datatable['R_scaled2B'], 'r-', label="R")
plt.plot(exp_datatable['Obsc'], exp_datatable['G_scaled2B'], 'g-', label="G")
plt.plot(exp_datatable['Obsc'], exp_datatable['B_scaled2B'], 'b-', label="B")
plt.plot(exp_datatable['Obsc'], exp_datatable['clouds'], 'k-', label="Clouds")
plt.xlabel("Eclipse Obscuration")
plt.ylabel("Color Intensity (scaled to B value)")
plt.title("Eclipse Obscuration versus Fisheye Color")
plt.show()

fig, ax = plt.subplots()
plt.plot(exp_datatable['Alt'], exp_datatable['lux'])
plt.xlabel("Sun's altitude")
plt.ylabel("Flux (lux)")
plt.title("Flux versus altitude (lux)")
plt.show()

fig, ax = plt.subplots()
plt.plot(dates_SQM, exp_datatable['R_interp'], 'r-', label="R")
plt.plot(dates_SQM, exp_datatable['G_interp'], 'g-', label="G")
plt.plot(dates_SQM, exp_datatable['B_interp'], 'b-', label="B")
plt.plot(dates_SQM, exp_datatable['clouds']*100, 'k-', label="Clouds")
plt.xlabel("UTC time")
plt.ylabel("Color Intensity (max=100)")
plt.title("Color versus Time (Interpolated)")
labels = ax.get_xticklabels()
formatter = mdates.DateFormatter('%H:%M:%S')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.setp(labels, rotation=30, fontsize=10)
plt.show()

fig, ax = plt.subplots()
plt.plot(dates_SQM, exp_datatable['R_scaled2B'], 'r-', label="R")
plt.plot(dates_SQM, exp_datatable['G_scaled2B'], 'g-', label="G")
plt.plot(dates_SQM, exp_datatable['B_scaled2B'], 'b-', label="B")
plt.plot(dates_SQM, exp_datatable['clouds'], 'k-', label="Clouds")
plt.xlabel("UTC time")
plt.ylabel("Color Intensity (scaled to B value)")
plt.title("Color versus Time (Interpolated)")
labels = ax.get_xticklabels()
formatter = mdates.DateFormatter('%H:%M:%S')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.setp(labels, rotation=30, fontsize=10)
plt.show()

fig, ax = plt.subplots()
plt.plot(dates_SQM, exp_datatable['R_scaled2G'], 'r-', label="R")
plt.plot(dates_SQM, exp_datatable['G_scaled2G'], 'g-', label="G")
plt.plot(dates_SQM, exp_datatable['B_scaled2G'], 'b-', label="B")
plt.plot(dates_SQM, exp_datatable['clouds'], 'k-', label="Clouds")
plt.xlabel("UTC time")
plt.ylabel("Color Intensity (scaled to G value)")
plt.title("Color versus Time (Interpolated)")
labels = ax.get_xticklabels()
formatter = mdates.DateFormatter('%H:%M:%S')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.setp(labels, rotation=30, fontsize=10)
plt.show()
