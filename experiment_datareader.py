#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Experiment Data Reader
Created on Tue June 27 2017 01:00:00 PM
Author: Juan Cabanela

This Python script reads the FITS files output by my experiment code...

It doesn't do much yet, it is really meant as a shell for later development of
python scripts for data analysis.
"""

import os
import numpy as np
from astropy.table import Table, Column
import explib as exper

# Greeting and attach to devices
print("")
print("Eclipse 2017 Reduced Data Reader")
print("================================")

# Get the reduced FITS datafile filename
filename = exper.get_reduced_datafile()

# Read in the original experimental data into astropy table
print(" > Loading datafile ...", end='')
full_datable = Table.read(filename, format='fits')
print(" DONE!")

# Build tables that are groups into wavelength data and other data
by_datatype = full_datable.group_by('datatype')
data_mask = by_datatype.groups.keys['datatype'] == 'data'
wl_mask = by_datatype.groups.keys['datatype'] == 'wavelength'

exp_datatable = by_datatype.groups[data_mask]
wl_datatable = by_datatype.groups[wl_mask]

# Example of loading ONLY clear sky data
clear_datable = exp_datatable[exp_datatable['clouds']==0]

# Load wavelength and spectral data into arrays, leave the rest in the
# astropy table
wavelengths = wl_datatable['spectrum'][0]  # 1D Array
spectra = np.array(exp_datatable['spectrum'])  # 2D Array

print("Full datatable:")
print(full_datable)
print("Experimental datatable:")
print(exp_datatable)
print("datatable columns:")
print(exp_datatable.colnames)
print("datatable SQM data in mpsas (Astropy data form):")
print(exp_datatable['mpsas'])
print("datatable SQM data in mpsas (np.array form):")
print(np.array(exp_datatable['mpsas']))
print("Wavelengths in spectra")
print(wavelengths)
print("Spectra Array")
print(spectra)
