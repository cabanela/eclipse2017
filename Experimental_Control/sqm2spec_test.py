#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SQM versus Spectrometer Test
Created on Fri Jun  2 13:26:50 2017
Author: Juan Cabanela

This Python script is being used to determine what the SQM-LU reads in a given
situation versus the maximum counts from the spectrometer.  This will be used
to determine an appropriate equation for taking an SQM-LU reading and turning
it into a suggested exposure time for the spectrometer.

June 22, 2017: Updated to store spectra as single astropy column entry, which
   then needs to be written out as a FITS file as well as a CSV file.  For this
   reason the code was updated to output data in BOTH file formats.

"""

import pysqm_mini as pysqm
import pysqm_additions as pysymadd
import speclib as spec
import specplotlib as splot
import datetime
import matplotlib.pyplot as plt
import numpy as np
from astropy.table import Table, Column


def mpsas2lux(mpsas):
    '''
    This function simply converts magnitudes per square arcsecond into linear
    units of lux or Candela per square meter.  Based on the function provided
    on page 12 of the SQM-LU User Manual
    '''
    return 1.08e5 * pow(10, -0.4*mpsas)

#
# Configure SQM settings
#
Number_measures = 5         # measurements per SQM reading
Pause_Measures = 3          # Inter-measurement pause (in sec) for SQM reading
time_between_readings = 15  # Seconds to wait between SQM readings
filter_offset = -4.85       # Shift in mpsas due to filter/tube (6/22/17 notes)
max_readings = 0            # Force this many readings then done (0 = ignore)
dk_corr = True             # Use dark corrections
data_dir = "../EclipseProjectData/CalibrationData/SQMvSpecData/"  # Path to data directory

# Greeting and attach to devices
print("")
print("SQM versus Spectrometer Test")
print("============================")

# Define data filename stub based on timestamp for later use
now = datetime.datetime.utcnow()
datafilename = now.strftime("sqmVspecdata-%Y%m%dUTC%H%M%S")

# Check data directory path
dd_last = data_dir.strip()[-1]
if (dd_last == "/"):
    print("> Data Directory set to " + data_dir, end="\n\n")
else:
    print("> Data Directory (" + data_dir + ") doesn't end with /, DEATH!!!",
          end="\n\n")
    sys.exit(-1)

# Open SQM device and print basic configuration information
print("> Attaching SQM (takes a few seconds)")
sqmdev = pysqm.SQMLU(verbose=False)
print("  SQM-LU Model: ", sqmdev.model_number,
      "   Serial Number: ", sqmdev.serial_number)
print("    Light Calibration Offset: ", sqmdev.light_cal_offset,
      " mpsas at ", sqmdev.light_cal_temp, "C")
print("    Assuming filter offset of {0:05.2f} mpsas".format(filter_offset))
print("")

# Open spectrometer and print basic configuration information
print("> Attaching Spectrometer")
specdev = spec.attach_spectometer(verbose=False)
print("  Spectrometer Model: ", specdev.model,
      "   Serial Number: ", specdev.serial_number)
nl_coeff = specdev._nc
if (nl_coeff[0] == 0):
    print("    Non-linearity coefficients NOT stored in spectrometer EPROM.")
    nl_corr = False
    nl_str = "F"
else:
    print("    Non-linearity coefficients ARE stored in spectrometer EPROM.")
    nl_corr = True
    nl_str = "T"

if dk_corr is False:
    print("    NOT applying dark current exposures.")
    dk_str = "F"
else:
    print("    Applying dark current exposures.")
    dk_str = "T"
print("")


#
# DETERMINE FILENAME TO USE
#
now = datetime.datetime.utcnow()
defname = now.strftime("sqmNspecdata-%Y%m%dUTC%H%M%S")

fname_descript = "Enter data filename stub (default: "+defname+"): "
filename = input(fname_descript)
if (filename == ""):
    filename = defname
print(" > Using datafile name: ", filename)

# Determine full path to data filename then build FITS and CSV filenames
data_fullpath = data_dir + filename
fitsfile = data_fullpath + ".fits"
csvfile = data_fullpath + ".csv"

#
# SET UP DATA TABLES
#
# Determine wavelengths of each channel
lamb, intens = spec.exposure(specdev, 0.05, darkcorr=dk_corr,
                             nlcorr=nl_corr, verbose=False)

# Build list of headers for each SQM and spectra taken
name_list_csv = ['UTC', 'mpsas', 'lux', 'SQM_Temp', 'darkcorr', 'nlcorr',
             'exposure']
dtype_list_csv = ['S22', 'f8', 'f8', 'f8', 'S1', 'S1', 'f8']
for wavelength in lamb:
    new_column = "{0:07.2f}nm".format(wavelength)
    name_list_csv.append(new_column)   # Add scalar to list
    dtype_list_csv.append("f8")        # Add scalar to list
sqmNspectra_csv = Table(names=name_list_csv, dtype=dtype_list_csv)

name_list_fits = ['UTC', 'datatype', 'mpsas', 'lux', 'SQM_Temp', 'darkcorr',
                  'nlcorr', 'exposure']
dtype_list_fits = ['S22', 'S10', 'f8', 'f8', 'f8', 'S1', 'S1', 'f8']
sqmNspectra_fits = Table(names=name_list_fits, dtype=dtype_list_fits)

# compute UTC now for use in 'wavelength' data row for FITS datafile
now = datetime.datetime.utcnow()
subsecond = ('{0:3.2f}'.format(round(now.microsecond/1000000, 2)))[1:5]
utc_string = now.strftime("%Y%m%d %H%M%S") + subsecond
datatype = "wavelength"
sqmNspectra_fits.add_row([utc_string, datatype, 0, 0, 0, dk_str, nl_str, 1])

# Now build wavelength row of data for storage as row in data file
nm = []
for wavelength in lamb:
    wavelength = float("{0:07.2f}".format(wavelength))
    nm.append(wavelength)   # Add scalar to list

# Build list of wavelengths into spectrum and add as column to Astropy table
spectrum = Column(data=[nm], name="spectrum")
sqmNspectra_fits.add_column(spectrum)


#
# Run through the process of collecting data and asking to save it
#
print("Proceeding to data capture:")
n = 0   # Number of loops attempted
datatype = "data"
try:
    while True:
        print("You have saved ", str(n)," readings so far...")
        # Request exposure length
        exp_str = input("Enter spectrometer exp. time (integer>=2, default 3): ")
        if (exp_str != ""):
            exp_time = int(exp_str)
            if (exp_time < 2):
                exp_time = 2
        else:
            exp_time = 3
        print(" > Setting spectrometer exposure time of ", exp_time, " sec.")

        # compute UTC at beginning of this data sample
        now = datetime.datetime.utcnow()
        subsecond = ('{0:3.2f}'.format(round(now.microsecond/1000000, 2)))[1:5]
        utc_string = now.strftime("%Y%m%d %H%M%S") + subsecond

        print(" > Getting readings from both devices:")
        # Get data from SQM
        print("   > Getting readings from SQM ({0:02d} sec)".format(
                (max(Pause_Measures,1)+1)*Number_measures))
        SQM_data = sqmdev.read_photometer(
                Nmeasures=Number_measures, PauseMeasures=Pause_Measures,
                offset_calibration=filter_offset)
        SQM2use = pysymadd.clean_output(SQM_data)
        temp = SQM2use[2]
        mpsas = SQM2use[5]
        lux = float(round(mpsas2lux(mpsas), 3))
        print("    > SQM {0:05.2f} mpsas ({1:05.3f} lux) at {2:+06.2f}C".format(
                mpsas, lux, temp))

        # Get data from spectrometer and display
        print("   > Getting readings from Spectrometer ({0:02d} sec)".format(
                exp_time))
        lamb, intens = spec.exposure(specdev, exp_time, darkcorr=False,
                                     nlcorr=nl_corr, verbose=False)
        splot.spectraplot(lamb, intens)
        print("    > Spectrum max counts: {0:05.2f}".format(np.nanmax(intens)))
        plt.show()

        # Ask if we should save this data
        ans = input(" > Save these results (Y/N): ")
        ans_first = ans.strip()[0].lower()
        if (ans_first == 'y'):
            # Save the results (CSV)
            row_list = [utc_string, mpsas, lux, temp, dk_str, nl_str, exp_time]
            row_list.extend(intens.tolist())   # append list to list
            sqmNspectra_csv.add_row(row_list)
            # Save the results (FITS)
            row_list_fits = [utc_string, datatype, mpsas, lux, temp, dk_str,
                             nl_str, exp_time, [intens.tolist()]]
            sqmNspectra_fits.add_row(row_list_fits)
            n += 1   # Increment the number of records saved

        # Ask if we should get another data sample
        ans = input(" > Try another pair of readings (Y/N): ")
        ans_first = ans.strip()[0].lower()
        if (ans_first == 'n'):
            break

        print("")
except:
    print("Caught Error or Interupt, closing Controllers and Saving File")

    # Detach the spectrometer
    spec.detach_spectometer(specdev)

    # Write out file
    sqmNspectra_csv.write(csvfile, format='csv', overwrite=True)
    sqmNspectra_fits.write(fitsfile, format='fits', overwrite=True)

    # Output report to screen and close
    print("> A total of ", n, " readings saved to ", csvfile)
    print("  and to ", fitsfile)

# Detach spectrometers
spec.detach_spectometer(specdev, verbose=False)

# Write out files
sqmNspectra_csv.write(csvfile, format='csv', overwrite=True)
sqmNspectra_fits.write(fitsfile, format='fits', overwrite=True)

# Output report to screen and close
print("> A total of ", n, " readings saved to ", csvfile)
print("  and to ", fitsfile)
print("")
print("DONE. Exiting SQM v Spectrometer Calibration")
