#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SQM controller
Created on Mon Mar 27 10:40:00 2017
Author: Juan Cabanela

This Python script is being used to testbed the access to the Unihedron
SQM data over the USB port using Python.

"""
import pysqm_mini as pysqm
import pysqm_additions as pysymadd
import time
import datetime
from astropy.table import Table

# Configure SQM settings
Number_measures = 5         # measurements per SQM reading
Pause_Measures = 3          # Inter-measurement pause (in sec) for SQM reading
time_between_readings = 15  # Seconds to wait between SQM readings
filter_offset = -4.85       # Shift in mpsas due to filter/tube (6/22/17 notes)
max_readings = 0            # Force this many readings (0 means ignore)

# Set data path
data_dir = "../EclipseProjectData/CalibrationData/SQMCalibration/" # Name of data directory

# Retrieve the maximum number of SQM readings to perform
print("")
print("SQM Controller")
print("==============")
max_string = input("Enter maximum number of readings (Hit <enter> for no limit): ")
if (max_string != ""):
    max_readings = int(max_string)
    print(" > Requesting ",max_readings," readings.")

# Open device and print basic configuration information
mydevice = pysqm.SQMLU()
print("")
print("SQM-LU Model: ", mydevice.model_number,"   Serial Number: ", mydevice.serial_number)
print("Light Calibration Offset: ", mydevice.light_cal_offset," mpsas at ", mydevice.light_cal_temp,"C")
print("")

# Define filename based on timestamp
now = datetime.datetime.utcnow()
sqm_filename = now.strftime("sqmdata-%Y%m%dUTC%H%M%S.csv")
sqm_fullpath = data_dir + sqm_filename

# Configure Astropy table
sqm_table= Table(names=('UTC', 'Localtime', 'Temp', 'Freq', 'SkyBrightness'),\
                 dtype=('S22','S22', 'f8', 'f8', 'f8'))
    
# Read and process the data from the SQM
if (max_readings>0):
    print("Collecting SQM readings every {0:02d} seconds until {1:02d} readings obtained.".format(time_between_readings,max_readings))
else:
    print("Collecting SQM reading every {0:02d} seconds until you hit 'Ctrl-C'".format(time_between_readings))
print(" > Assuming filter offset of {0:05.2f} mpsas".format(filter_offset))
print(" > Using SQM datafile name: ",sqm_filename)

try:
    n_reading = 0
    # Execute forever or while less than the max number of readings
    while (n_reading < max_readings and max_readings > 0) or (max_readings == 0):
        SQM_data = mydevice.read_photometer(\
                     Nmeasures=Number_measures, PauseMeasures=Pause_Measures, offset_calibration=filter_offset)
        n_reading += 1
        SQM2use = pysymadd.clean_output(SQM_data)
        print(" >  Reading {0:04d}: {1:05.2f} mpsas at {2:>22} ({3:+06.2f}C)".format(n_reading,SQM2use[5],SQM2use[1],SQM2use[2]))
        # Save data to Astropy table
        sqm_table.add_row([SQM2use[0], SQM2use[1], SQM2use[2], SQM2use[3], SQM2use[5]])
        # Wait for next reading
        time.sleep(time_between_readings)
        
    # Write out file
    print("Writing ",sqm_fullpath, " ", end='')
    sqm_table.write(sqm_fullpath, format='csv')
    print("(DONE!)")
    
except (KeyboardInterrupt, SystemExit):
    print("Caught Keyboard Interupt, closing SQM Controller")
    
    # Write out file
    print("Writing ",sqm_fullpath, " ", end='')
    sqm_table.write(sqm_fullpath, format='csv')
    print("(DONE!)")
    
except:
    # report error and proceed       
    print("Crashed with error, exiting SQM Controller")
    raise
    
print("Exiting SQM Controller")