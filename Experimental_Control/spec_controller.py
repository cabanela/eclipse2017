#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Spec controller
Created on Tue Mar 14 15:20:26 2017
Author: Juan Cabanela

This Python script is being used to testbed the access to the Ocean Optics
USB2000 spectrometer via Python with the eventual goal of generating a script
to automatically handle sky spectrum acquisition during the August 21, 2017
total eclipse.

"""
import matplotlib.pyplot as plt
import speclib as spec
import specplotlib as splot

# Attach the spectrometer
dev = spec.attach_spectometer()
# Determine number of pixels (for use in setting array sizes)
channels = dev.pixels

# Run through a series of exposures with the spectrometer
n = 0   # Number of loops attempted
time = float(input("Enter the exposure time (in sec): "))

# Retrieve data from spectrometer WITH dark corrections
lamb, intens = spec.exposure(dev, time, darkcorr=True, nlcorr=True, verbose=True)

# Detach the spectrometer
spec.detach_spectometer(dev)

# Plot up the spectrum
#plt.plot(lamb, intens, label='{0:0.2f} seconds'.format(time))
#plt.xlabel("Wavelength (nm)")
#plt.ylabel("Intensity (with dark-correction)")
#plt.legend()
splot.spectraplot(lamb, intens)
plt.show()
