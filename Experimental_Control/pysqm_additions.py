#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PySQM_Additions
Created on Fri Jun  2 13:29:56 2017
Author: Juan Cabanela

This library contains code that isn't directly from PySQM but is used in the
operation of the SQM.
"""

import pysqm_mini as pysqm
import time
import datetime

def clean_output(SQMdata):
    """
    This function "cleans up" the formatting of the output from PySQM for
    better printing and storage.  Turns them all into strings.
    
        SQM_data contains
        [0]: timeutc_mean
        [1]: timelocal_mean
        [2]: temp_sensor (Temperature measured at light sensor in degrees C)
        [3]: freq_sensor (Frequency of sensor in Hz)
        [4]: ticks_uC  (Period of sensor in counts, counts occur at a rate of 460.8 kHz (14.7456MHz/32).)
        [5]: sky_brightness (mag/arcsec^2, 0.00 returned if above upper brightness limit)

        From the manual:
          Each SQM-LU is factory-calibrated. The absolute precision of each
          meter is believed to be ±10% (±0.10 mag/arcsec^2 ). The diﬀerence 
          in zero-point between each calibrated meter is typically ±10% 
          (±0.10 mag/arcsec^2)
    """
    date_format = "%Y%m%d %H%M%S" # Date formatting string
    
    SQMdata2print = list(SQMdata)  # Clone the list
    
    # Round times to hundredths of a second (and leaves trailing 0's in place)
    digits_to_show = 2
    subsecond = ('{0:3.2f}'.format(round(SQMdata[0].microsecond/1000000, digits_to_show)))[1:3+digits_to_show]
    SQMdata2print[0] = SQMdata[0].strftime(date_format)+subsecond
    subsecond = ('{0:3.2f}'.format(round(SQMdata[1].microsecond/1000000, digits_to_show)))[1:3+digits_to_show]
    SQMdata2print[1] = SQMdata[1].strftime(date_format)+subsecond
    
    # Round temperature to 0.1C
    SQMdata2print[2] = float(round(SQMdata[2], 1))

    # Round frequency to 1Hz
    SQMdata2print[3] = float(round(SQMdata[3], 1))
    
    # Convert period to float
    SQMdata2print[4] = float(SQMdata[4])
    
    # Round sky brightness to 0.01 mag/arcsec^2
    SQMdata2print[5] = float(round(SQMdata[5], 2))
    
    return(SQMdata2print)
