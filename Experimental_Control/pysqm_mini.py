#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PySQM Mini

This is a subset of the PySQM package for use reading the SQM-LU, tweaked 
to run as a seperate library and to support Python 3.

Also added
* searching for ports under Darwin/macos
* stripped out anything dealing with site specific information or 
  file handling for data storage.
* Added handling of local timezones using built-in instead of site-specific
  configurations.
* Added subroutine to process calibration data and set SQM variables to hold
  that information, added the call so this occurs during initialization of 
  SQMLU object.

"""
import numpy as np
import serial as serial
import glob
import sys
import time
import datetime

# Default, no debugging.
DEBUG=False

# Default, to ignore the length of the read string.
_cal_len_  = None
_meta_len_ = None
_data_len_ = None


def remove_linebreaks(data):
    # Remove line breaks from data
    data = data.replace('\r\n','')
    data = data.replace('\r','')
    data = data.replace('\n','')
    return(data)


def format_value(data,remove_str=' '):
    # Remove string and spaces from data
    data = remove_linebreaks(data)
    data = data.replace(remove_str,'')
    data = data.replace(' ','')
    return(data)


def format_value_list(data,remove_str=' '):
    # Remove string and spaces from data array/list
    data = [format_value(line,remove_str).split(';') for line in data]
    return(data)


def set_decimals(number,dec=3):
        str_number = str(number)
        int_,dec_ = str_number.split('.')
        while len(dec_)<=dec:
            dec_=dec_+'0'

        return(int_+'.'+dec_[:dec])


def filtered_mean(array,sigma=3):
    # Our data probably contains outliers, filter them
    # Notes:
    #   Median is more robust than mean
    #    Std increases if the outliers are far away from real values.
    #    We need to limit the amount of discrepancy we want in the data (20%?).

    # We will use data masking and some operations with arrays. Convert to numpy.
    array = np.array(array)

    # Get the median and std.
    data_median = np.median(array)
    data_std    = np.std(array)

    # Max discrepancy we allow.
    fixed_max_dev  = 0.2*data_median
    clip_deviation = np.min([fixed_max_dev,data_std*sigma])

    # Create the filter (10% flux + variable factor)
    filter_values_ok = np.abs(array-data_median)<=clip_deviation
    filtered_values = array[filter_values_ok]

    # Return the mean of filtered data or the median.
    if np.size(filtered_values)==0:
        print('Warning: High dispersion found on last measures')
        filtered_mean = data_median
    else:
        filtered_mean = np.mean(filtered_values)

    return(filtered_mean)


class SQM(object):
    def read_datetime(self):
        # Get UTC datetime from the computer.
        utc_dt = datetime.datetime.utcnow()
        return(utc_dt)

    def local_datetime(self,utc_dt):
        # Get timezone offset from the computer and apply it to convert
        # input UTC time to local time
        offset = time.timezone if (time.localtime().tm_isdst == 0) else time.altzone
        return(utc_dt + datetime.timedelta(seconds=-offset))
    
    def read_photometer(self,Nmeasures=1,PauseMeasures=2,offset_calibration=0,verbose=False):
        # Initialize values
        temp_sensor   = []
        flux_sensor   = []
        freq_sensor   = []
        ticks_uC      = []
        Nremaining = Nmeasures

        # Promediate N measures to remove jitter
        timeutc_initial = self.read_datetime()
        while(Nremaining>0):
            InitialDateTime = datetime.datetime.now()

            # Get the raw data from the photometer and process it.
            raw_data = self.read_data(tries=10)
            temp_sensor_i,freq_sensor_i,ticks_uC_i,sky_brightness_i = \
             self.data_process(raw_data)

            temp_sensor += [temp_sensor_i]
            freq_sensor += [freq_sensor_i]
            ticks_uC    += [ticks_uC_i]
            flux_sensor += [10**(-0.4*sky_brightness_i)]
            Nremaining  -= 1
            DeltaSeconds = (datetime.datetime.now()-InitialDateTime).total_seconds()

            # Just to show on screen that the program is alive and running
            if (verbose):
                sys.stdout.write('>')
                sys.stdout.flush()

            # Force a pause of at least one second
            if (Nremaining>0): time.sleep(max(1,PauseMeasures-DeltaSeconds))
        
        # Provide feedback to screen
        if (verbose):
            sys.stdout.write(' ')
            sys.stdout.flush()
            
        timeutc_final = self.read_datetime()
        timeutc_delta = timeutc_final - timeutc_initial

        timeutc_mean   = timeutc_initial+\
         datetime.timedelta(seconds=int(timeutc_delta.seconds/2.+0.5))
        timelocal_mean = self.local_datetime(timeutc_mean)

        # Calculate the mean of the data.
        temp_sensor = filtered_mean(temp_sensor)
        freq_sensor = filtered_mean(freq_sensor)
        flux_sensor = filtered_mean(flux_sensor)
        ticks_uC    = filtered_mean(ticks_uC)
        sky_brightness = -2.5*np.log10(flux_sensor)

        # Correct from offset (if cover/filter is installed on the photometer)
        sky_brightness = sky_brightness+offset_calibration
        
        # Provide feedback to screen
        if (verbose):
            print(timelocal_mean, sky_brightness)
        
        return(timeutc_mean, timelocal_mean, temp_sensor, freq_sensor, ticks_uC, sky_brightness)

    def metadata_process(self,msg,sep=','):
        # Separate the output array in items
        msg = format_value(msg)
        msg_array = msg.split(sep)
        
        # Get Photometer identification codes
        self.protocol_number = int(format_value(msg_array[1]))
        self.model_number    = int(format_value(msg_array[2]))
        self.feature_number  = int(format_value(msg_array[3]))
        self.serial_number   = int(format_value(msg_array[4]))


    def calibration_process(self,msg,sep=','):
        # Separate the output array in items
        msg = format_value(msg)
        msg_array = msg.split(sep)

        # Output definition characters
        cal_char  = 'm'
        dtime_char = 's'
        ltemp_char = 'C'
        offset_char = 'm'
        dtemp_char = 'C'

        # Get the Calibration parameters
        self.light_cal_offset = float(format_value(msg_array[1],cal_char))
        self.light_cal_temp = float(format_value(msg_array[3],ltemp_char))
        self.dark_cal_time = float(format_value(msg_array[2],dtime_char))
        self.dark_cal_temp = float(format_value(msg_array[5],dtemp_char))
        self.light_offset = float(format_value(msg_array[4],offset_char))
        

    def data_process(self,msg,sep=','):
        # Separate the output array in items
        msg = format_value(msg)
        msg_array = msg.split(sep)

        # Output definition characters
        mag_char  = 'm'
        freq_char = 'Hz'
        perc_char = 'c'
        pers_char = 's'
        temp_char = 'C'

        # Get the measures
        sky_brightness = float(format_value(msg_array[1],mag_char))
        freq_sensor    = float(format_value(msg_array[2],freq_char))
        ticks_uC       = float(format_value(msg_array[3],perc_char))
        period_sensor  = float(format_value(msg_array[4],pers_char))
        temp_sensor    = float(format_value(msg_array[5],temp_char))

        # For low frequencies, use the period instead
        if freq_sensor<30 and period_sensor>0:
            freq_sensor = 1./period_sensor

        return(temp_sensor,freq_sensor,ticks_uC,sky_brightness)

    def start_connection(self):
        ''' Start photometer connection '''
        pass

    def close_connection(self):
        ''' End photometer connection '''
        pass

    def reset_device(self):
        ''' Restart connection'''
        self.close_connection()
        time.sleep(1)
        #self.__init__()
        self.start_connection()


class SQMLU(SQM):
    def __init__(self, verbose=True):
        '''
        Search the photometer and
        read its metadata
        
        You can define the global variable config_device_addr with the 
        SQM-LU device address in it.
        '''
        if (verbose):
            print("Initializing SQMLU:")
        try:
            #print('Trying fixed device address %s ... ' %str(config_device_addr))
            self.addr = config_device_addr
            self.bauds = 115200
            self.start_connection()
        except:
            #print('Trying auto device address ... ', end='')
            self.addr = self.search()
            #print('%s found' %str(self.addr))
            self.bauds = 115200
            self.start_connection()
        if (verbose):
            print(' > %s found' %str(self.addr))
            
        # Clearing buffer
        if (verbose):
            print(' > Clearing buffer ...', end='')
        buffer_data = self.read_buffer()
        time.sleep(1)
        #print(buffer_data, end='')
        
        ## Turn off ideal crossover firmware since it seems to interfere with
        ## reliable reading of metadata and calibration data.
        if (verbose):
            print(' DONE')
            print(' > Turning off ideal crossover firmware  ...', end='')
        self.yx_readout = self.ideal_crossover_firmware_off()
        time.sleep(1)
        buffer_data = self.read_buffer()  # Just in case spurious data is left
         
        # Reading metadata and calibration data
        if (verbose):
            print(' DONE')
            print(' > Reading metadata and calibration info ...', end='')
        self.ix_readout = self.read_metadata(tries=10)
        time.sleep(1)
        self.cx_readout = self.read_calibration(tries=10)
        time.sleep(1)
        if (verbose):
            print(" DONE")
        ## Turn on ideal crossover firmware
        #if (verbose):
        #    print(' DONE')
        #    print(' > Turning on ideal crossover firmware ...')
        #self.yx_readout = self.ideal_crossover_firmware_on()

        if (verbose):
            print('   > Reading data (test) ...', end='')
        self.rx_readout = self.read_data(tries=10)
        if (verbose):
            print(" DONE")


    def search(self):
        '''
        Photometer search.
        Name of the port depends on the platform.
        '''
        ports_darwin = glob.glob('/dev/tty.usbserial-FT*')
        ports_unix = ['/dev/ttyUSB'+str(num) for num in range(100)]
        ports_win  = ['COM'+str(num) for num in range(100)]

        os_in_use = sys.platform

        if os_in_use == 'linux2':
            if (DEBUG):
                print('Detected Linux platform')
            ports = ports_unix
        elif os_in_use == 'darwin':
            if (DEBUG):
                print('Detected Darwin platform')
            ports = ports_darwin
        elif os_in_use == 'win32':
            if (DEBUG):
                print('Detected Windows platform')
            ports = ports_win

        if (DEBUG):
            print("Searching ports:", ports)
            
        used_port = None
        for port in ports:
            conn_test = serial.Serial(port, 115200, timeout=1)
            # Python 3 change: send as byte-literal
            conn_test.write(b'ix')
            bytes_read = conn_test.readline()
            string_read = bytes_read.decode('UTF-8')
            if string_read[0] == 'i':
                used_port = port
                break

        try:
            assert(used_port!=None)
        except:
            print('Error: SQM Device not found!  Exiting...')
            print("")
            sys.exit()
        else:
            return(used_port)

    def start_connection(self):
        '''Start photometer connection '''

        self.s = serial.Serial(self.addr, 115200, timeout=2)

    def close_connection(self):
        ''' End photometer connection '''
        # Check until there is no answer from device
        request = ""
        r = True
        while r:
            r = self.read_buffer()
            request += str(r)

        self.s.close()

    def reset_device(self):
        ''' Connection reset '''
        #print('Trying to reset connection')
        self.close_connection()
        self.start_connection()

    def read_buffer(self):
        ''' Read the data '''
        msg = None
        try: msg = self.s.readline()
        except: pass
        # Python 3 compability change, modify so string returned instead of
        # byte-literal.
        return(msg.decode('UTF-8'))

    def ideal_crossover_firmware_on(self):
        ''' 
        Enables ideal crossover firmware which optimizes the frequency
        the SQM reads
        
        Turn on with "YCx", reply is YRcpu
        Turn off with "Ycx", reply is YRCpu
        Check status with "Yx"?
        
        THIS FUNCTION IS NOT RELIABLE IN CHECKING STATUS OF FIRMWARE.
        '''
        read_err = False
        try: self.s.write(b'YCx')
        except: pass
        time.sleep(1)
        
        # Check status is firmware off
        try: self.s.write(b'Yx')
        except: pass
        msg = self.read_buffer()
        try:
            assert("YRcpu" in msg)
        except:
            read_err=True
 
        # Check that msg contains data
        if read_err==True:
            print('ERR. Turning on ideal crossover firmware failed: %s' %str(msg))
            if (DEBUG): raise
            return(-1)
        else:
            if (DEBUG):
                print('Sensor info: '+str(msg), end='')
            return(msg)

    def ideal_crossover_firmware_off(self):
        ''' 
        Disables ideal crossover firmware which optimizes the frequency
        the SQM reads
        
        Turn on with "YCx", reply is YRcpu
        Turn off with "Ycx", reply is YRCpu
        Check status with "Yx"?
        
        THIS FUNCTION IS NOT RELIABLE IN CHECKING STATUS OF FIRMWARE.
        '''
        read_err = False
        try: self.s.write(b'Ycx')
        except: pass
        time.sleep(1)
        
        # Check status is firmware off
        try: self.s.write(b'Yx')
        except: pass
        msg = self.read_buffer()
#        try:
#            assert("YRCpu" in msg)
#        except:
#            read_err=True    
#        # Check that msg contains data
#        if read_err==True:
#            print('ERR. Turning off ideal crossover firmware failed: %s' %str(msg))
#            if (DEBUG): raise
#            return(-1)
#        else:
#            if (DEBUG):
#                print('Sensor info: '+str(msg), end='')
#            return(msg)
        return(msg)
        
    def read_metadata(self,tries=1):
        ''' Read the serial number, firmware version '''
        # Python 3 change: send as byte-literal
        self.s.write(b'ix')
        time.sleep(1)
        read_err = False
        msg = self.read_buffer()

        # Check metadata
        try:
            # Sanity check
            assert(len(msg)==_meta_len_ or _meta_len_==None)
            assert("i," in msg)
            self.metadata_process(msg)
        except:
            tries-=1
            read_err=True

        if (read_err==True and tries>0):
            print("Error, reseting device...")
            time.sleep(1)
            self.reset_device()
            time.sleep(1)
            msg = self.read_metadata(tries)
            if (msg!=-1): read_err=False

        # Check that msg contains data
        if read_err==True:
            print('ERR. Reading the photometer!: %s' %str(msg))
            if (DEBUG): raise
            return(-1)
        else:
            if (DEBUG):
                print('Sensor info: '+str(msg), end='')
            return(msg)

    def read_calibration(self,tries=1):
        ''' Read the calibration data '''
        # Python 3 change: send as byte-literal
        self.s.write(b'cx')
        time.sleep(1)

        read_err = False
        msg = self.read_buffer()

        # Check caldata
        try:
            # Sanity check
            assert(len(msg)==_cal_len_ or _cal_len_==None)
            assert("c," in msg)
            self.calibration_process(msg)
        except:
            tries-=1
            read_err=True

        if (read_err==True and tries>0):
            time.sleep(1)
            self.reset_device()
            time.sleep(1)
            msg = self.read_calibration(tries)
            if (msg!=-1): read_err=False

        # Check that msg contains data
        if read_err==True:
            print('ERR. Reading the photometer!: %s' %str(msg))
            if (DEBUG): raise
            return(-1)
        else:
            if (DEBUG):
                print('Calibration info: '+str(msg), end='')
            return(msg)

    def read_data(self,tries=1):
        ''' Read the SQM and format the Temperature, Frequency and NSB measures '''
        # Python 3 change: send as byte-literal
        self.s.write(b'rx')
        time.sleep(1)

        read_err = False
        msg = self.read_buffer()

        # Check data
        try:
            # Sanity check
            assert(len(msg)==_data_len_ or _data_len_==None)
            assert("r," in msg)
            self.data_process(msg)
        except:
            tries-=1
            read_err=True

        if (read_err==True and tries>0):
            time.sleep(1)
            self.reset_device()
            time.sleep(1)
            msg = self.read_data(tries)
            if (msg!=-1): read_err=False

        # Check that msg contains data
        if read_err==True:
            print('ERR. Reading the photometer!: %s' %str(msg))
            if (DEBUG): raise
            return(-1)
        else:
            if (DEBUG): print('Data msg: '+str(msg))
            return(msg)
