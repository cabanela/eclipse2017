#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Spectroneter Bad Column ID
Created on Wed July 12, 2017 10:12 AM
Author: Juan Cabanela

This Python script (based on spec_controller.py) is used to read the spectrum
and then it tries to identify the bad columns.

Approach is to simply compare the counts with the adjoining columns and see if 
they differ by more than 82 acounts (2% of the entire 4096 register).  If I
test the spectrometer using a 'blackbody' source, this should work.

Save the bad columns (and spectrometer serial number) to a file.

"""
import numpy as np
import matplotlib.pyplot as plt
import speclib as spec
import specplotlib as splot
from astropy.stats import sigma_clip

n_stack = 10   # Number of spectra to stack
percent = 0.025  # Allowed difference (in percentage of 4096)
    
# Attach the spectrometer
dev = spec.attach_spectometer()
# Determine number of pixels (for use in setting array sizes)
channels = dev.pixels
# Save the serial number
serial = dev.serial_number

# Run through a series of exposures with the spectrometer
time = float(input("Enter the exposure time (in sec): "))

# Retrieve first spectra from spectrometer without dark corrections
print("> Getting spectra 1 (of {0:d})".format(n_stack))
lamb, intens = spec.exposure_raw(dev, time, darkcorr=False, nlcorr=True, verbose=False)

# Loop and add
intens_stack = intens
for n in np.arange(1, n_stack):
    print("> Getting spectra {0:d} (of {1:d})".format(n+1, n_stack))
    lamb, intens = spec.exposure_raw(dev, time, darkcorr=False, nlcorr=True, verbose=False)
    intens_stack += intens    
intens_stack /= n_stack

# Detach the spectrometer
spec.detach_spectometer(dev)

# Plot up the spectrum
splot.spectraplot(lamb, intens_stack)
plt.ylim( (0, 5000) )
plt.title("Raw Spectrum (average of {0:d} spectra)".format(n_stack))

# Now attempt to identify the bad columns and label them
num_to_side = 3  # Check this number of columns to each side (if possible)

# WARNING: The following approach is quick but 'wraps around' the initial
# and final columns.  I discovered the counts don't quite line up, so I have
# mark bad data with nan to no include it in averages/std.
#
stats_array = np.zeros((2*num_to_side, intens_stack.size))
for i in np.arange(1, num_to_side+1):
    stats_array[i-1] = np.roll(intens_stack, i)
    stats_array[-i] = np.roll(intens_stack, -i)  # reverse rolled intens to back
    # fix wrapped columns
    for j in np.arange(1, i+1):
        stats_array[i-1][j-1] = np.NaN
        stats_array[-i][-j] = np.NaN

# Determine averages and std. dev. of adjacent points (unused)
adj_means = np.nanmean(stats_array, axis=0)
adj_stds = np.nanstd(stats_array, axis=0)

# Sigma_clipped stats
clipped_stats_array = sigma_clip(stats_array, sigma=2, axis=0)
clipped_adj_means = np.ma.average(clipped_stats_array,axis=0)
clipped_adj_stds = np.ma.std(clipped_stats_array,axis=0)

# if the clipped std dev is 0, use unclipped std dev, since this indicates
# redundant values in input and accept the original std. dev. instead
bad_points =  np.where(clipped_adj_stds == 0)
clipped_adj_stds[bad_points] = adj_stds[bad_points]

# Check for columns differing from adjacent columns in unappealing way
# WARNING: Returns a tuple of an array, so to pull just array get [0] element
bad_cols = np.where(np.abs(intens_stack - clipped_adj_means) > 4096*percent)[0]
good_cols = np.where(np.abs(intens_stack - clipped_adj_means) <= 4096*percent)[0]

# Label the bad columns with down arrows
arrow = u'$\u2193$'
plt.plot(lamb[bad_cols], 4500*np.ones(lamb[bad_cols].size), linestyle='none', 
         marker=arrow, markersize=10)
plt.show()

# Plot up Cleaned spectrum
splot.spectraplot(lamb[good_cols], intens_stack[good_cols])
plt.title("Spectrum (cleaned)")
plt.show()

# Print list of bad columns
bad_cols_str = ', '.join(['{0:d}'.format(num) for num in bad_cols])
print("Bad columns for Spectrometer ", serial, " appear to be ", bad_cols_str)
print("Maximum counts (from good columns):", np.max(intens_stack[good_cols]) )

# Ask if we should save this data
ans = input(" > Save bad column data (Y/N)?")
ans_first = ans.strip()[0].lower()
if (ans_first == 'y'):
    # Save the results (FITS)
    outfile = spec.badcol_write(serial, bad_cols)
    print("  > bad columns data saved to ",outfile)
