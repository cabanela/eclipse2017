#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Spec library
Author: Juan Cabanela

This Python library contains some functions I have created for using the Ocean
Optics spectrometers via python.  
"""

## This block of code is designed to check if there is a RedTide USB650 
## attached and if there is, it uses the pyUSB interface, otherwise it
## uses the C interface.  It requires pyusb be installed even if you don't
## use it.
##
## If you are using the Ocean Optics USB2000, you can comment out the entire
## block and avoid the need to install pyusb.
#import usb.core
#import seabreeze
#if usb.core.find(idVendor=0x2457, idProduct=0x1014):
#    # If we find a connected USB650
#    seabreeze.use("pyseabreeze")
#else:
#    seabreeze.use("cseabreeze")
## End of block to determine if RedTide USB650 is installed

# Windows 10 attempt (forces pyUSB useage)
#import usb.core
#import seabreeze
#seabreeze.use("pyseabreeze")

import seabreeze.spectrometers as sb
import sys
import math as math
import numpy as np
import os.path

def attach_spectometer(verbose=True):
    """
    Identify first spectrometer that is attached and select it.

    Parameters:
    verbose (Boolean): Be verbose (default: True)

    Returns:
    spec: pyseabreeze spectrometer object
    """

    if (verbose):
        print("Initializing Spectrometer:")

    try:
        devices = sb.list_devices()
        dev = sb.Spectrometer(devices[0])
        if (verbose):
            print(' > %s found' % str(devices[0]))
    except:
        sys.exit()

    if (verbose):
        # Print specifications
        print("attach_spectometer: Attaching ", dev.model, " serial number ", dev.serial_number)

    return dev


def detach_spectometer(dev, verbose=True):
    """
    Detach spectrometer that is attached.

    Parameters:
    spec: pyseabreeze spectrometer object
    verbose (Boolean): Be verbose (default: True)

    Returns:
    spec: pyseabreeze spectrometer object
    """
    if (verbose):
        # Print specifications
        print("detach_spectometer: Detaching ", dev.model, " serial number ", dev.serial_number)
    dev.close()
    return


def exposure_raw(dev, itime, darkcorr=True, nlcorr=True, verbose=False):
    """
    Operate the spectrometer and obtain an exposure using itime seconds
    integration time (raw data, no removal of bad data)

    Parameters:
    dev (spectrometer): pyseabreeze spectrometer object
    itime (float): Integration time in seconds)
    darkcorr (Boolean):  Apply dark current corrections (default: True)
    nlcorr (Boolean):  Apply non-linearity corrections (default: True)

    Returns:
    wavelengths: Array of wavelengths in nm
    intensities: Array of intensities (in counts)
    """

    # Define the integration time
    if (itime == ""):
        itime = 1
    else:
        itime = float(itime)

    itime_actual = int(itime * 1e6)    # integration time in microseconds
    if (verbose):
        print("> expose: Integration time of ", itime_actual, " microseconds.")

    # PySeabreeze limits integration time to range(3000, 655350000).
    mintime = dev.minimum_integration_time_micros
    maxtime = 65535 * 10000
    if (itime_actual < mintime or itime_actual > maxtime):
        print("Error: Integration time must be ", mintime, " to ", maxtime, " microseconds.")
        quit()

    # Set the integration time on the spectrometer then retrieve the spectrum
    # This exception catching was added when initial testing triggered errors
    # in the open source SeaBreeze C library.
    try:
        dev.integration_time_micros(itime_actual)
    except Exception as e:
        print("Error: Seabreeze returns ERROR_CODE: %d" % e.error_code)
        raise

    # Check and make sure non-linearity coefficients are stored in device
    nl_coeff = dev._nc
    if ((nl_coeff[0] == 0) and (nlcorr)):
        raise Exception('No non-linearity coefficients stored in device EPROM')

    # determine wavelengths and intensities
    wvlen = dev.wavelengths()
    if (verbose):
        if (darkcorr):
            print("> expose: Applying dark current corrections.")
        else:
            print("> expose: NOT applying dark current corrections.")
        if (nlcorr):
            print("> expose: Applying non-linearity corrections.")
        else:
            print("> expose: NOT applying non-linearity corrections.")
    intens = dev.intensities(correct_dark_counts=darkcorr, correct_nonlinearity=nlcorr)

    # Return the wavelengths and counts
    return wvlen, intens


def exposure(dev, itime, darkcorr=True, nlcorr=True, verbose=False):
    """
    Operate the spectrometer and obtain an exposure using itime seconds
    integration time (remove known bad columns) using exposure_raw

    Parameters:
    dev (spectrometer): pyseabreeze spectrometer object
    itime (float): Integration time in seconds)
    darkcorr (Boolean):  Apply dark current corrections (default: True)
    nlcorr (Boolean):  Apply non-linearity corrections (default: True)

    Returns:
    wavelengths: Array of wavelengths in nm
    intensities: Array of intensities (in counts)
    """
    
    # Take the raw exposure
    wvlen_raw, intens_raw = exposure_raw(dev, itime, darkcorr=darkcorr, 
                                                nlcorr=nlcorr, verbose=verbose)
    
    # Read bad column data
    bad_col = badcol_read(dev.serial_number)
    
    # Determine good columns
    all_col = np.arange(intens_raw.size)
    good_col = np.in1d( all_col, bad_col, invert=True)
    
    # Return the wavelengths and counts
    return wvlen_raw[good_col], intens_raw[good_col]


def badcol_write(serial, badcol_array, data_dir="./"):
    """
    Write the stored bad column data into the data dir.
    """
    # Determine filename based on serial number
    filename = "badcols-{0:s}.csv".format(serial)
    csvfile = data_dir + filename
    
    bad_cols_str = ','.join(['%d' % num for num in badcol_array])
    file_handle = open(csvfile, "w")
    file_handle.write(bad_cols_str)
    file_handle.close()
    return csvfile


def badcol_read(serial, data_dir="./", verbose=True):
    """
    Read the stored bad column data in the data dir
    """
    # Determine filename based on serial number
    filename = "badcols-{0:s}.csv".format(serial)
    csvfile = data_dir + filename
    
    # Read file if it exists
    if (os.path.isfile(csvfile)):    
        file_handle = open(csvfile, "r")
        bad_cols_str = file_handle.readline()  # Read the one row of data
        file_handle.close()
    else:
        if (verbose):
            print("Warning: No saved bad column data for spectrometer ",serial)
        bad_cols_str = ""
        
    return np.fromstring(bad_cols_str, sep=',', dtype='int')
