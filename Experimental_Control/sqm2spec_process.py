#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SQM lux versus Spectroscopic exposure rate Data Processor
This version created on Tue June 20, 2017
Author: Juan Cabanela

June 20, 2017 Version:
This first version of this Python script is based off speclinearity_process.py
and is designed to read in the CSV files of SQM versus spectrometer counts
as built using sqm2spec_test.py and allow me to process the results.

June 23, 2017 Version:
Modified to work with input data files in FITS format.  Hopeing this will
result in a marked speed increase.

July 14, 2017 Version:
Added line fitting for the SQM lux versus 99th percentile of the spectrometer 
counts.
"""
import os
import re

import matplotlib.pyplot as plt
import numpy as np
from astropy.table import Table, Column


def mpsas2lux(mpsas):
    '''
    This function simply converts magnitudes per square arcsecond into linear
    units of lux or Candela per square meter.  Based on the function provided
    on page 12 of the SQM-LU User Manual
    '''
    return 1.08e5 * pow(10, -0.4*mpsas)


# Get inputs from user
print("SQM versus Spectrometer Data Processor")
print("======================================")
print()

# Data file locations
data_dir = "../EclipseProjectData/CalibrationData/SQMvSpecData/"  # Name of data directory

print("Default data directory ", data_dir)
print("SQM2spec CSV files in directory (from oldest to youngest) are:")
# Get list of datafiles
# Build a list of files using generator (can only run generator in parenthesis
# onces)
entries = list(os.path.join(data_dir, fn) for fn in os.listdir(data_dir))
# Search for file of specific name within datafiles
datafiles = [file for file in entries if "sqmNspecdata" in file]
fitsfiles = [file for file in datafiles if "fits" in file]
fitsfiles.sort(key=lambda x: os.path.getmtime(x))
defname = fitsfiles[-1]
for fname in fitsfiles:
    print("  ", fname)

fname_descript = "Enter datafile name (default: "+defname+"): "
filename = input(fname_descript)
if (filename == ""):
    filename = defname
print(" > Using ", filename, " for data.")

# Read in the data
print(" > Loading datafile ...", end='')
sqm2spec_orig = Table.read(filename, format='fits')
print(" DONE!")

# Process the data
print(" > Computing the count rates and building new table ...", end='')

# Read column names
colnames = sqm2spec_orig.colnames

# Create new astropy.Table with rates data in it and then append non-float
# columns to this new table at beginning (go through these columns in reverse
# order since I am adding them to the front individually)
ratestable = sqm2spec_orig.copy()

# Compute count rates for each row here, also compute stats here, start with
# blank lists
medians = []
mins = []
maxs = []
q1bound = []
q3bound = []
last1bound = []
for row in range(len(ratestable)):
    # Fix possible exposure time error if exposure time on wavelength line set
    # to zero instead of one.
    if (ratestable['datatype'][row] != 'data'):
        ratestable['exposure'][row] = 1

    # Compute the count rates
    ratestable['spectrum'][row] /= ratestable['exposure'][row]

    # Compute the count rate stats
    if (ratestable['datatype'][row] == 'data'):
        theserates = np.sort(ratestable['spectrum'][row])
        medians.append(np.median(theserates))
        mins.append(theserates[0])
        maxs.append(theserates[-1])
        q1bound.append(np.percentile(theserates, 25))
        q3bound.append(np.percentile(theserates, 75))
        last1bound.append(np.percentile(theserates, 99))
    else:
        # append bogus data to wavelength record
        medians.append(-999)
        mins.append(-999)
        maxs.append(-999)
        q1bound.append(-999)
        q3bound.append(-999)
        last1bound.append(-999)


# Add the statistics columns to the ratestable
ratestable['median'] = Column(medians)
ratestable['min'] = Column(mins)
ratestable['max'] = Column(maxs)
ratestable['1st_quartile'] = Column(q1bound)
ratestable['3rd_quartile'] = Column(q3bound)
ratestable['99th_percentile'] = Column(last1bound)

# Rename column in new table
ratestable.rename_column('spectrum', 'spectrum_rate')
print(" DONE!")

# Correct the SQM values (and lux) due to error of using original April 12
# value for the filter_offset, I will need to disable this code in future
# Original correction was -2.49, current correct is -4.85, meaning the SQM
# needs to drop an additional 2.36.
correct_filter_offset = False
if (correct_filter_offset):
    print(" > WARNING: DON'T do the following for data post June 22, 2016.")
    print(" > WARNING: Applying filter_offset correction ... ", end='')
    ratestable['mpsas'] -= 2.36
    ratestable['lux'] = mpsas2lux(ratestable['mpsas'])
    print("DONE!")
    for row in range(len(ratestable)):
        # Compute the count rate stats
        if (ratestable['datatype'][row] == 'wavelength'):
            ratestable['mpsas'][row] = 0
            ratestable['lux'][row] = 0

# Save output file
outfilename = re.sub('sqmNspecdata', 'sqmNspecrates', filename)
print("Saving processed data to ", outfilename, " ...", end='')
ratestable.write(outfilename, format='fits', overwrite=True)
print("DONE!")

# Build tables that are groups into wavelength data and spectral data
rates_by_datatype = ratestable.group_by('datatype')
data_mask = rates_by_datatype.groups.keys['datatype'] == 'data'
wl_mask = rates_by_datatype.groups.keys['datatype'] == 'wavelength'

rates_data = rates_by_datatype.groups[data_mask]
wl_data = rates_by_datatype.groups[wl_mask]

# Fit the 99th percentile versus lux
coeff, cov = np.polyfit(rates_data['lux'], rates_data['99th_percentile'], 1, cov=True)
coeff_err = np.sqrt(np.diag(cov))
[m, b] = coeff
[delta_m, delta_b] = coeff_err

print("Plotting results ... ")
print("Fit parameters for 99th percentile line are:")
print("     {0:5.3f}+-{1:5.3f} * lux + {2:4.0f}+-{3:4.0f}".format(m, delta_m, b, delta_b))

# Generate some plots of the trends between lux and the statistics
plt.clf()
plt.plot(rates_data['lux'], rates_data['1st_quartile'], 'bo',
         label="1st Quartile")
plt.plot(rates_data['lux'], rates_data['3rd_quartile'], 'ro',
         label="3rd Quartile")
plt.plot(rates_data['lux'], rates_data['99th_percentile'], 'go',
         label="99th Percentile")
plt.plot(rates_data['lux'], m*rates_data['lux'] + b, '-', label="99th percentile (fit)")
plt.xlabel("lux")
plt.ylabel("Count Rate (counts/sec)")
plt.title('Count Rate vs. Exposure')
plt.legend(loc='upper left')
plt.show()

## Generate some plots of the trends between SQM and the statistics
#plt.clf()
#plt.plot(rates_data['mpsas'], rates_data['1st_quartile'], 'bo',
#         label="1st Quartile")
#plt.plot(rates_data['mpsas'], rates_data['3rd_quartile'], 'ro',
#         label="3rd Quartile")
#plt.plot(rates_data['mpsas'], rates_data['99th_percentile'], 'go',
#         label="99th Percentile")
#plt.xlabel("SQM (mpsas)")
#plt.ylabel("Count Rate (counts/sec)")
#plt.title('Count Rate vs. Exposure')
#plt.legend(loc='upper right')
#plt.show()
#
## Plot spectra sorted by lux
#plt.clf()
#
## Extract the list of wavelengths from wavelength data (assume only one
## line of data)
#wavelength = wl_data['spectrum_rate'][0]
#
## Plot the individual count rates
#sorti = rates_data.argsort(keys='lux')
#for row in sorti:
#    plt.plot(wavelength, rates_data['spectrum_rate'][row],
#             label='{0:04.2f} lux'.format(ratestable['lux'][row]))
#plt.xlabel("wavelength")
#plt.ylabel("Count Rate (counts/sec)")
#plt.title('Spectral Count Rate vs. Lux')
#plt.legend()
#plt.show()
