#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Experiment Controller
Created on Wed June 15 2017 02:15:00 PM
Author: Juan Cabanela

This Python script (based on a version of the SQM to Spectroscopic exposure
calibration code) is designed to actually run my Eclipse 2017 experiment.  It
will, in a FULLY AUTOMATED fashion:

1) Connect to both SQM and spectroscope.  Then pause until experiment started.
2) Repeat the following procedure to obtain SQM/Spectral readings
    - Take the SQM reading
    - Append the SQM reading to astropy table
    - Every X loops, dump SQM astropy table to disk with timestamp-based name
    - Compute the expected exposure needed for the spectroscope
    - Determine time of beginning of exposure and take spectroscopic reading
    - Append spectroscopic reading to astropy table
    - Every X loops,dump spectra astropy table to disk w/ timestamp-based name
3) With a keyboard interupt:
    - Dump out final SQM astropy data table to disk
    - Dump out final spectral astropy data table to disk
    - Close connections to devices
    - Quit.

June 23, 2017: Modifying to put the combined SQM and spectrometer output into
    a single FITS file, while keeping the separated CSV data output files.
    - Also added a very basic (returning a fixed value) exposure estimator.

July 18, 2017: Modified the code, finishing out mpsas2lux function to return
   zero exposure time for spectrometer if the counts get too low.  I won't be
   able to run during totality, but can likely get close.
   - I also turned on dark corrections.

July 20, 2017: Updated the exp_estimate code to reflect results from my first
   test run at dusk, developing a new relationship for counts using the real
   sky.

   Also, fixed a bug that dropped the exposure time to zero it if was actually
   in the ideal range.

July 27, 2017: Modifed code slightly to include exposure time estimation based 
    on noise estimates.

Remaining (potential) modifications:
** Potentially may want to perform a pre-read of spectrometer that gets
   tossed out after changing the exposure length, since there appears to be
   no stability scan.
"""

import pysqm_mini as pysqm
import pysqm_additions as pysymadd
import speclib as spec
import specplotlib as splot
import sys
import time
import datetime
import numpy as np
from astropy.table import Table, Column


def mpsas2lux(mpsas):
    """
    This function simply converts magnitudes per square arcsecond into linear
    units of lux or Candela per square meter.  Based on the function provided
    on page 12 of the SQM-LU User Manual
    """
    return 1.08e5 * pow(10, -0.4*mpsas)


def exp_estimate(lux, verbose=True):
    """
    Use the lux value to estimate the necessary exposure time for the
    spectrometer and estimate the expected signal to noise ratio.
    """
    min_lux = 1          # Based on previous experience, set a hard limit
    min_exp = 0.00001    # Minimum allowed exposure time (seconds)
    best_exp = 0.85      # Ideal exposure time (seconds)
    max_exp = 20.0       # Maximum allowed exposure time (seconds)
    max_count = 3000     # Maximum desired counts
    min_count = 1500     # Minimum desired counts
    absmin_count = 1000  # Minimum allowed counts

    # Fit parameters from  99th percentile of counts sqm2spec_test
    m = 10
    b = 65

    # Estimate the count rate (counts/sec for 99th percentile)
    est_countrate = m*lux+b
    
    # Determine what the estimate count rate is if we use an ideal exposure
    # time (based on keeping spectral noise to a minimum)
    ideal_count = est_countrate * best_exp

    # Determine the exposure length
    exp_to_use = 0  # Assume no exposure
    if lux < min_lux:
        # If the sky is too dim, just skip the exposure
        return exp_to_use, 1

    if ideal_count > max_count:
        # If counts are going to be too high, just scale it down a bit
        ratio = ideal_count / max_count
        exp_to_use = best_exp / ratio
        # If exposure is too short, try with minimum exposure
        if exp_to_use < min_exp:
                exp_to_use = min_exp
    elif ideal_count < min_count:
        # If counts are going to be too low, just scale up the counts
        # get above the minimum we need, as long as we don't exceed the
        # maximum exposure length.
        ratio = ideal_count / min_count
        exp_to_use = best_exp / ratio
        # Desired exposure length is too long, check if we can eek out results
        # using the maximum exposure length, otherwise, set to zero.
        if exp_to_use > max_exp:
            if max_exp*est_countrate > absmin_count:
                exp_to_use = max_exp
            else:
                exp_to_use = 0
    else:
        # Use the ideal exposure time
        exp_to_use = best_exp

    if verbose:
        print("   > {0:05.1f}lux -> exp_time:{1:05.2f}s -> max_count:{2:05.1f}"
              .format(lux, exp_to_use, exp_to_use * est_countrate))

    # Estimate the noise to 90th percentile ratio at this exposure
    # This estimate was based on dusk measurements of noise between 800nm
    # and 1000nm compared to the 90th percentile value across entire spectrum.
    noise_est = np.power(10, -0.925 * np.log10(lux))
    
    return exp_to_use, noise_est


def UTC_to_hundredths():
    """
    Return the UTC and local time to hundredths of a second.
    """
    now = datetime.datetime.utcnow()
    subsecond = ('{0:3.2f}'.format(round(now.microsecond/1000000, 2)))[1:5]

    return now.strftime("%Y%m%d %H%M%S") + subsecond


def local_time():
    # Get timezone offset from the computer and apply it to convert
    # input UTC time to local time
    if time.localtime().tm_isdst == 0:
        offset = time.timezone
    else:
        offset = time.altzone
    return datetime.datetime.utcnow() + datetime.timedelta(seconds=-offset)


#
# Configure SQM settings
#
show_plot = True            # Display spectral plot or not?
Number_measures = 5         # measurements per SQM reading
Pause_Measures = 1          # Inter-measurement pause (in sec) for SQM reading
time_between_loops = 30     # Minimum time between loops with spectra
alttime_between_loops = 10  # Minimum time between loops skipping spectra
filter_offset = -4.85       # Shift in mpsas due to filter/tube (6/22/17 notes)
max_readings = 0            # Force this many readings then done (0 = ignore)
dk_corr = True              # Use dark corrections
nloops4dumps = 5            # number of loops between data writes
data_dir = "../EclipseProjectData/ExperimentData/"  # Name of data directory

# Greeting and attach to devices
print("")
print("Eclipse 2017 Experiment Controller")
print("==================================")

# Open SQM device and print basic configuration information
print("> Minimum time between start of loops set to {0:f} sec"
      .format(time_between_loops))
print("> Dumping data to disk every {0:d} loops (minimum spacing of {1:f} sec)"
      .format(nloops4dumps, nloops4dumps * time_between_loops))
dd_last = data_dir.strip()[-1]
if dd_last == "/":
    print("> Data Directory set to " + data_dir, end="\n\n")
else:
    print("> Data Directory (" + data_dir + ") doesn't end with /, DEATH!!!",
          end="\n\n")
    sys.exit(-1)

print("> Attaching SQM (takes a few seconds)")
sqmdev = pysqm.SQMLU(verbose=False)
print("  SQM-LU Model: ", sqmdev.model_number,
      "   Serial Number: ", sqmdev.serial_number)
print("    Light Calibration Offset: ", sqmdev.light_cal_offset,
      " mpsas at ", sqmdev.light_cal_temp, "C")
print("    Assuming filter offset of {0:05.2f} mpsas".format(filter_offset))
print("")

# Open spectrometer and print basic configuration information
print("> Attaching Spectrometer")
specdev = spec.attach_spectometer(verbose=False)
print("  Spectrometer Model: ", specdev.model,
      "   Serial Number: ", specdev.serial_number)
nl_coeff = specdev._nc
if nl_coeff[0] == 0:
    print("    Non-linearity coefficients NOT stored in spectrometer EPROM.")
    nl_corr = False
    nl_str = "F"
else:
    print("    Non-linearity coefficients ARE stored in spectrometer EPROM.")
    nl_corr = True
    nl_str = "T"

if dk_corr is False:
    print("    NOT applying dark current exposures.")
    dk_str = "F"
else:
    print("    Applying dark current exposures.")
    dk_str = "T"
print("")
print("WARNING: Be sure to TURN OFF SLEEP before running this script!")

# Start the experiment
ans = input(" > Is this setup OK for the experiment (Y/N): ")
ans_first = ans.strip()[0].lower()
if ans_first != 'y':
    # Detach the spectrometer and SQM
    spec.detach_spectometer(specdev)
    sqmdev.close_connection()

    # Exit script
    sys.exit(0)
else:
    print("")


# SET UP DATA TABLES
#

# Configure Astropy table for SQM data
sqm_table = Table(names=('loop', 'UTC_SQM', 'Localtime_SQM', 'Temp', 'Freq',
                         'mpsas', 'lux'),
                  dtype=('i2', 'S22', 'S22', 'f8', 'f8', 'f8', 'f8'))

# Determine wavelengths of each spectrometer channel
lamb, intens = spec.exposure(specdev, 0.05, darkcorr=dk_corr,
                             nlcorr=nl_corr, verbose=False)

# Save blank spectrum for later use
intens0 = 0*intens

# Configure Astropy table for Spectral data CSV file
name_list = ['loop', 'UTC_Spec', 'darkcorr', 'nlcorr', 'exposure']
dtype_list = ['i2', 'S22', 'S1', 'S1', 'f8']
for wavelength in lamb:
    new_column = "{0:07.2f}nm".format(wavelength)
    name_list.append(new_column)   # Add scalar to list
    dtype_list.append("f8")        # Add scalar to list
spectra_table_csv = Table(names=name_list, dtype=dtype_list)

# Configure Astropy table for merged data FITS file
name_list_fits = ['loop', 'UTC_SQM', 'Localtime_SQM', 'Temp', 'Freq', 'mpsas',
                  'lux', 'datatype', 'UTC_Spec', 'darkcorr', 'nlcorr',
                  'exposure']
dtype_list_fits = ['i2', 'S22', 'S22', 'f8', 'f8', 'f8', 'f8', 'S10', 'S22',
                   'S1', 'S1', 'f8']
merged_table_fits = Table(names=name_list_fits, dtype=dtype_list_fits)

# compute UTC now for use in 'wavelength' data row for FITS datafile
utc_string = UTC_to_hundredths()
datatype = "wavelength"
merged_table_fits.add_row([-1, utc_string, '-', 0, 0, 0, 0, datatype,
                           utc_string, dk_str, nl_str, 0])

# Now build wavelength row of data for storage as row in data file
nm = []
for wavelength in lamb:
    wavelength = float("{0:07.2f}".format(wavelength))
    nm.append(wavelength)   # Add scalar to list

# Build list of wavelengths into spectrum and add as column to Astropy table
spectrum = Column(data=[nm], name="spectrum")
merged_table_fits.add_column(spectrum)

#
# Run through the process of collecting data and asking to save it
#
print("Proceeding with data capture (hit Control-C ONCE to quit):")
n = 0   # Initialize Number of loops
datatype = "data"
try:
    while True:
        start_loop_time = time.time()  # Keep track of elapsed time
        n += 1  # Increment loop counter
        # Get local time and note the start of loop
        local = local_time()
        local_str = local.strftime("%H:%M:%S")
        print("")
        print('Starting Loop {0:d} at {1:s}:'.format(n, local_str))
        print("(hit Control-C ONCE to quit):")
        print(" > Getting readings from both devices:")

        # Get data from SQM
        print("   > Getting SQM reading (~{0:02d} sec)".format(
                (max(Pause_Measures, 1)+1)*Number_measures))
        SQM_data = sqmdev.read_photometer(
                Nmeasures=Number_measures, PauseMeasures=Pause_Measures,
                offset_calibration=filter_offset)
        SQM2use = pysymadd.clean_output(SQM_data)
        lux = float(round(mpsas2lux(SQM2use[5]), 3))
        print("      > SQ {0:05.2f} mpsas ({1:05.3f} lux) at {2:+06.2f}C"
              .format(SQM2use[5], lux, SQM2use[2]))

        # Save data to SQM data table in memory
        sqm_data = [n, SQM2use[0], SQM2use[1], SQM2use[2], SQM2use[3],
                    SQM2use[5], lux]
        sqm_table.add_row(sqm_data)

        # Write to file if appropriate
        if n % nloops4dumps == 0:
            # Construct filename
            now = datetime.datetime.utcnow()
            sqm_filename = now.strftime("exp_sqmdata-%Y%m%dUTC%H%M%S.csv")
            sqm_fullpath = data_dir + sqm_filename
            # Write the file
            sqm_table.write(sqm_fullpath, format='csv', overwrite=True)
            print("      > cumulative SQM data saved to ", sqm_filename)

        # compute UTC at beginning of this spectrum
        utc_string = UTC_to_hundredths()

        # Determine the exposure time based on SQM reading information
        exp_to_use, N_est = exp_estimate(lux, verbose=False)

        # Get spectrum
        if exp_to_use > 0:
            print("   > Integrating Spectra (~{0:05.2f} sec)".format(exp_to_use))
            print("     with estimated noise {0:06.4f}x 90th percentile".format(N_est))
            lamb, intens = spec.exposure(specdev, exp_to_use, darkcorr=dk_corr,
                                         nlcorr=nl_corr, verbose=False)
            # Show plot on screen
            if (show_plot):
                splot.spectraplot(lamb, intens)
                
            intens_sorted = np.sort(intens)
            last1per = np.percentile(intens_sorted, 99)
            print("      > 99th percentile counts: {0:05.2f}".format(last1per))
        else:
            # No exposure possible, generate blank spectrum
            print("   > NO Spectra obtained, sky too dim")
            intens = intens0

        # Save spectral data to astropy tables in memory
        row_list_core = [utc_string, dk_str, nl_str, exp_to_use]
        row_list_csv = [n] + row_list_core  # Copy (without reference) the list
        row_list_csv.extend(intens.tolist())   # append list to list
        spectra_table_csv.add_row(row_list_csv)

        row_list_fits = sqm_data + [datatype] + row_list_core
        row_list_fits.extend([[intens.tolist()]])
        merged_table_fits.add_row(row_list_fits)

        # Write to file if appropriate
        if n % nloops4dumps == 0:
            # Write the CSV file
            now = datetime.datetime.utcnow()
            spec_filename_csv = now.strftime("exp_specdata-%Y%m%dUTC%H%M%S.csv")
            spec_fullpath_csv = data_dir + spec_filename_csv

            spectra_table_csv.write(spec_fullpath_csv, format='csv',
                                    overwrite=True)
            print("      > cumulative spectral data saved to")
            print("         ", spec_filename_csv)

            # Write the FITS file
            filename_fits = now.strftime("exp_data-%Y%m%dUTC%H%M%S.fits")
            fullpath_fits = data_dir + filename_fits
            merged_table_fits.write(fullpath_fits, format='fits',
                                    overwrite=True)
            print("      > cumulative merged data saved to")
            print("         ", filename_fits)

        # Wait for next reading (shorter time if no spectra was obtained)
        elapsed_time = time.time() - start_loop_time
        if exp_to_use > 0:
            pause = max(0, (time_between_loops - elapsed_time))
        else:
            pause = max(0, (alttime_between_loops - elapsed_time))
        print(' > Elapsed time {1:05.2f}s in loop {0:04d} (pausing {2:05.2f}s)'
              .format(n, elapsed_time, pause))
        time.sleep(pause)

except KeyboardInterrupt:
    print("")
    print("NOTICE: Caught Interrupt, closing Controllers and Saving Files.")

    # Write out datafiles
    now = datetime.datetime.utcnow()
    sqm_filename = now.strftime("exp_sqmdata-FINAL-%Y%m%dUTC%H%M%S.csv")
    sqm_fullpath = data_dir + sqm_filename
    sqm_table.write(sqm_fullpath, format='csv', overwrite=True)
    print("   > FINAL cumulative SQM data saved to ")
    print("      ", sqm_filename)

    spec_filename_csv = now.strftime("exp_specdata-FINAL-%Y%m%dUTC%H%M%S.csv")
    spec_fullpath_csv = data_dir + spec_filename_csv
    spectra_table_csv.write(spec_fullpath_csv, format='csv', overwrite=True)
    print("   > FINAL cumulative spectral data saved to")
    print("      ", spec_filename_csv)

    filename_fits = now.strftime("exp_data-FINAL-%Y%m%dUTC%H%M%S.fits")
    fullpath_fits = data_dir + filename_fits
    merged_table_fits.write(fullpath_fits, format='fits', overwrite=True)
    print("   > FINAL cumulative merged data saved to")
    print("      ", filename_fits)

    # Output report to screen
    print("> A total of ", len(spectra_table_csv), " SQM and Spectra readings saved.")

    # Detach the spectrometer and SQM
    print("   > Detaching spectrometer")
    spec.detach_spectometer(specdev)
    print("   > Detaching SQM")
    sqmdev.close_connection()

    # Print statement
    print("> Interrupt handling complete.\n")

# Output closing of code to screen
print("DONE. Exiting Eclipse 2017 Experiment")
