#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Fisheye Reader
Created on Mon October 16, 2017 10:44 PM
Author: Juan Cabanela

This Python script reads PNG images I took using a fisheye lens and will 
eventually compute the average color value in the middle of the image.
"""

import os
import numpy as np
from PIL import Image
import re
import sys

def get_image(image_path):
    """
    Get a numpy array of an image so that one can access values[x][y].
    
    From https://stackoverflow.com/questions/138250/
       how-can-i-read-the-rgb-value-of-a-given-pixel-in-python
    """
    image = Image.open(image_path, 'r')
    width, height = image.size
    pixel_values = list(image.getdata())
    if image.mode == 'RGB':
        channels = 3
    elif image.mode == 'L':
        channels = 1
    else:
        print("Unknown mode: %s" % image.mode)
        return None
    pixel_values = np.array(pixel_values).reshape((width, height, channels))
    return pixel_values


# Greeting and attach to devices
print("")
print("Eclipse 2017 Fish Eye Image Reader")
print("=====================================")

# Hard code the data directory location for now
data_dir = "../EclipseProjectData/FisheyePhotos/"

# Hard code data output filename
outfile = "../EclipseProjectData/ExperimentData/fisheye_colors.csv"

# Check data directory for validity
dd_last = data_dir.strip()[-1]
if (dd_last == "/"):
    print("> Data Directory set to " + data_dir, end="\n\n")
else:
    print("> Data Directory (" + data_dir + ") doesn't end with /, DEATH!!!",
          end="\n\n")
    sys.exit(-1)

# Build a list of PNG files generated during the Eclipse
entries = list(os.path.join(data_dir, fn) for fn in os.listdir(data_dir))
# Search for file of specific name within datafiles
jpgfiles = [file for file in entries if "jpg" in file]
jpgfiles.sort(key=lambda x: os.path.getmtime(x))
defname = jpgfiles[-1]

print("Creating ", outfile)
print("")
fh = open(outfile, "w")
outstring = "UTC,width,R_avg,G_avg,B_avg\n"
fh.write(outstring)

for fname in jpgfiles:
    print("> Loading ", fname)
    img_array = get_image(fname)

    # Process the image
    (x, y, z) = img_array.shape

    # Middle widthXwidth pixels
    width = 50
    midx_lo = int(x/2 - width/2)
    midx_hi = int(x/2 + width/2) + 1
    midy_lo = int(y/2 - width/2)
    midy_hi = int(y/2 + width/2) + 1
 
    # Average RGB values
    avg_r = np.average(img_array[midx_lo:midx_hi, midy_lo:midy_hi, 0])
    avg_g = np.average(img_array[midx_lo:midx_hi, midy_lo:midy_hi, 1])
    avg_b = np.average(img_array[midx_lo:midx_hi, midy_lo:midy_hi, 2])

    # Convert filname into UTC timestamp
    match = re.search(r".*\/([0-9 ]+)\.jpg", fname)
    dateinfo = match[1]
    disdate = np.array([int(dateinfo[0:4]), int(dateinfo[4:6]),
                        int(dateinfo[6:8]), int(dateinfo[9:11])+5,
                        int(dateinfo[11:13]), int(dateinfo[13:15])])
    # Convert to string, fixing a 1 hour error in timestamp due to camera
    # time setting error.
    datestr = "{0:04d}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}".format(disdate[0], disdate[1], disdate[2], disdate[3], disdate[4], disdate[5])

    outstring = "{0:s},{1:d},{2:4.1f},{3:4.1f},{4:4.1f}\n".format(datestr, width, avg_r, avg_g, avg_b)
    print(outstring)
    fh.write(outstring)
    fh.flush()

fh.close()
