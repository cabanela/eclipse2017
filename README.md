# Eclipse 2017 Repository #

This software repository includes all the software I have been developing for my Eclipse 2017 observing project. In includes software to automate data gathering from a Sky Quality Meter (SQM) measuring the sky brightness and an Ocean Optics USB2000 Spectrometer measuring the sky spectra.  I wanted to automate the data gathering so I could enjoy the eclipse without worrying about controlling the equipment.

The data gathering was a success even if it was hampered by clouds.  The resulting data files are on my home machine.  As I move toward data reduction, I am restructuring these files to focus on reduction of the data.

### Code Description ###

In main directory:

- *experiment_datareader.py*: This script reads the FITS files produced by *experiment_controller.py*.  This is basically
just a testbed script for reading the data.

- *specplotlib.py*: This script is a library of commands for plotting up the spectra from the experiment.

- *badcols-[].csv*: These are CSV files continaing the list of bad columns for any spectrometers I tested.

In **Experimental Control** directory:

This scripts would need to be moved into the same directory to the main directory to run properly since they require the bad column information and plotting library there:

- *pysqm_mini.py and pysqm_additions.py*: These two python libraries provide support for controlling the SQM-LU from Python.  They are based on the PySQM package (which is a self-containing set of Python programs for running an SQM monitoring station).  However, I removed support for the SQM-LE and database storage of data and added support for Python 3.x and some additional data cleaning.

- *sqm_controller.py*: This is just a basic script for controlling the SQM-LU. It just cycles through gatherine SQM readings and saving the results to a data file.

- *speclib.py*: A library of functions for accessing the Ocean Optics USB2000 spectrometer using the seabreeze library.  This includes some functions for connecting/disconnecting to/from the spectrometer and performing exposures.  It handles the bad columns if you have identified them.

- *spec_badcolumnid.py*: This script is used to try to automatically identify bad columns in an Ocean Optics USB2000 spectrometer.  It saves the bad column information so it can be used by the rest of the code accessing the spectrometer.

- *spec_controller.py*: This is just a basic script for controlling the Ocean Optics USB2000 spectrometer.  It takes an exposure and plots the spectrum.  It doesn't save the results.

- *speclinearity_test.py*: This script was designed to measure the amount of dark current for the spectrometer.  It automated the repeated exposures of various lengths and saving the results.  The data is analyzed by running *speclinearity_stats.py* and then *speclinearity_process.py* to see the results.  Because their was some concern about the order of operations, there is a version called *speclinearity_test_reverse.py* that ran the exposures from long to short.

- *sqm2spec_test.py*: This script takes sequential exposures with the SQM-LU and then spectrometer to allow calibration of the sky brightness values from the SQM to the spectrometer counts rate, so I can make predictions of the count rate for a given sky brightness.  Plot the results of the test using *sqm2spec_test.py*.

- *experiment_controller.py*: This is the goal, the main script that controls the experiment for measuring the sky brightness and color during the eclipse.

- *experiment_sqm2spec_test.py*: This script was used to review the results of an *experiment_controller.py* run to recalibrate the SQM to Spectrometer count rate relationship using the real sky data.


### Who do I talk to? ###

* Juan Cabanela (cabanela@mnstate.edu)
