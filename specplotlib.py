#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Specplot library
Author: Juan Cabanela

This Python library contains some functions I have created for plotting up 
spectral data.
"""

import matplotlib.pyplot as plt
import math as math
import numpy as np

"""
Code for making spectral plots in color is based on code found at
http://codingmess.blogspot.com/2009/05/conversion-of-wavelength-in-nanometers.html
"""

def wav2RGB(wavelength):
    """
    Input wavelength is in nanometers.

    Return R, G, and B values.
    """
    w = int(wavelength)

    # colour
    if w >= 380 and w < 440:
        R = -(w - 440.) / (440. - 350.)
        G = 0.0
        B = 1.0
    elif w >= 440 and w < 490:
        R = 0.0
        G = (w - 440.) / (490. - 440.)
        B = 1.0
    elif w >= 490 and w < 510:
        R = 0.0
        G = 1.0
        B = -(w - 510.) / (510. - 490.)
    elif w >= 510 and w < 580:
        R = (w - 510.) / (580. - 510.)
        G = 1.0
        B = 0.0
    elif w >= 580 and w < 645:
        R = 1.0
        G = -(w - 645.) / (645. - 580.)
        B = 0.0
    elif w >= 645 and w <= 780:
        R = 1.0
        G = 0.0
        B = 0.0
    else:
        R = 0.0
        G = 0.0
        B = 0.0

    # intensity correction
    if w >= 380 and w < 420:
        SSS = 0.3 + 0.7*(w - 350) / (420 - 350)
    elif w >= 420 and w <= 700:
        SSS = 1.0
    elif w > 700 and w <= 780:
        SSS = 0.3 + 0.7*(780 - w) / (780 - 700)
    else:
        SSS = 0.0
    SSS *= 255

    return [int(SSS*R), int(SSS*G), int(SSS*B)]


def wav2HEX(wavelength):
    """
    Input wavelength is in nanometers.

    Return HEX code.
    """

    (R,G,B) = wav2RGB(wavelength)
    return '#{0:02X}{1:02X}{2:02X}'.format(R, G, B)


def spectraplot_um_noshow(wavelength, fluxes):
    """
    Input wavelength numpy array in microns and
          fluxes in numpy array of fluxes in aribtrary units

    Inputs are assumed to be sorted by wavelength AND fluxs correspond
    to wavelengths
    """
    minw = 0.1*math.floor(wavelength[0]*10)
    maxw = 0.1*math.ceil(wavelength[-1]*10)

    maxflux = np.nanmax(fluxes)

    plt.xlim( (minw, maxw) )
    plt.ylim( (0, maxflux) )

    for i, flux in enumerate(fluxes):
        w = wavelength[i]
        w_color = wavelength[i]*1000
        plt.plot([w,w],[0,flux],color=wav2HEX(w_color))

    plt.xlabel("Wavelength (nm)")
    plt.ylabel("Flux")
    return

def spectraplot_noshow(wavelength, fluxes):
    """
    Input wavelength numpy array in nanometers and
          fluxes in numpy array of fluxes in aribtrary units

    Inputs are assumed to be sorted by wavelength AND fluxs correspond
    to wavelengths
    """
    minw = 10*math.floor(wavelength[0]/10)
    maxw = 10*math.ceil(wavelength[-1]/10)

    maxflux = np.nanmax(fluxes)

    plt.xlim( (minw, maxw) )
    plt.ylim( (0, maxflux) )

    for i, flux in enumerate(fluxes):
        w = wavelength[i]
        plt.plot([w,w],[0,flux],color=wav2HEX(w))

    plt.xlabel("Wavelength (nm)")
    plt.ylabel("Flux")
    return

def spectraplot(wavelength, fluxes):
    """
    This version generates the plot and displays it to screen.
    
    """
    spectraplot_noshow(wavelength, fluxes)
    
    plt.show()
    return